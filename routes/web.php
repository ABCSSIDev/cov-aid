<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('facebook',function(){
    return view('facebook');
});

Route::group(['prefix' => 'admin'],function (){
    Route::get('/', 'Admin\HomeController@index');
    Route::get('/donation/list','Admin\DonationController@donationList')->name('get.donations.list');
    Route::resource('/donation','Admin\DonationController');
    Route::get('/donation/image/{file}','Admin\DonationController@getImage')->name('donation.image');
    Route::get('/donation/verify-show/{id}','Admin\DonationController@verifyDonation')->name('verify.show');
});

Route::get('auth/facebook','FacebookController@redirectToFacebook');
Route::get('auth/facebook/callback','FacebookController@handleCallback');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/update/image/{file}','HomeController@getImage')->name('update.image');

//Route::get('/admin', 'HomeController@admin')->name('admin');
Route::get('/donation/success/{donate_id}','DonationController@donationSuccess')->name('donate.success');
Route::resource('/donate', 'DonationController');
Route::get('/donors', 'DonationController@showDonors')->name('show.donors');
Route::resource('/fundraising', 'FundraisingController');
Route::get('/account/image/{file}','FundraisingController@getImage')->name('account.image');
Route::get('/gcash', 'DonationController@gcash')->name('gcash');
Route::get('/bank', 'DonationController@bank')->name('bank');
Route::resource('/covid_data', 'CovidDataController');
Route::resource('/proof_donation', 'DonationProofController');
Route::get('/cases', 'CovidDataController@getCovidCasesChart')->name('covid.cases');
Route::resource('/update', 'UpdateController');
Route::get('/updates','FundraisingController@formUpdates')->name('updates.create');
Route::post('/updates/create','FundraisingController@storeUpdates')->name('updates.store');




