<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DonationProof extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'id';

    protected $fillable = ['donation_id', 'attachment', 'verified_by','verified_date','donation_method'];
    protected $dates = ['deleted_at'];

    public function donorData(){
        return $this->belongsTo('App\Donation','donation_id');
    }

    public function verifiedBy(){
        return $this->belongsTo('App\User','verified_by');
    }
}
