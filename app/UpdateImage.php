<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UpdateImage extends Model
{
    use SoftDeletes;
    protected $primaryKey = 'id';

    protected $fillable = ['subject', 'image_url', 'update_id','sort_order'];
    protected $dates = ['deleted_at'];
    

    public function updateDetails(){
        return $this->belongsTo('App\Update','update_id');
    }
}
