<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Donation extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'id';

    protected $fillable = ['user_id', 'amount', 'comment','hide_amount','hide_info','hide_comment','created_at'];
    protected $dates = ['deleted_at'];

    public function userData(){
        return $this->belongsTo('App\User','user_id');
    }

    public function getProof(){
        return $this->hasOne(DonationProof::class,'donation_id');
    }
}
