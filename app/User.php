<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use Notifiable;


    const ADMIN_TYPE = 1;
    const DEFAULT_TYPE = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','image','type','email', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function addNew($input){
        $check = static::where('facebook_id',$input['facebook_id'])->first();

        if(is_null($check)){
            return static::create($input);
        }


        return $check;
    }

    public static function getUserID(){
        $statement = DB::select("show table status like 'users'");
        return $statement[0]->Auto_increment;
    }

    public function isAdmin(){
        return $this->type === self::ADMIN_TYPE;
    }


    public static function getUser($user){
        return static::where('id',$user)->first();
    }

    public static function getImage($user_id){
        $user = static::getUser($user_id);

//
        if ($user->image == null){
            $userImage = asset('images/default_1.png');
        }elseif (strpos($user->image,'facebook')){
            $userImage = $user->image;
        }else{
            $userImage = route('account.image',$user_id);
        }

        return $userImage;
    }
}
