<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Donation;
use App\DonationProof;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Mail\Donated;
use Illuminate\Support\Facades\Mail;
use Illuminate\Database\Eloquent\Builder;

class DonationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $user = auth()->user();
        return view('donate.index',['user' => $user]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $user = User::all();
            if(count($user)!= 0){
                $user_data = DB::table('users')->orderby('id','desc')->first();
                $last_id = $user_data->id;
            }
            else{
                $last_id = 0;
            }
            if (Auth::check()){
                $user = Auth::user();
            }else{
                if($request->input('first_name')){
                    $name= $request->input('first_name').' '.$request->input('last_name');
                }
                else{
                    $name = 'Anonymous';
                }

                if($request->has('image')){
                    $id = DB::table('users')->orderby('id','desc')->first();
                    $base64_str = substr($request->image, strpos($request->image, ",")+1);
                    $image = base64_decode($base64_str);
                    $filename = "accounts-".time().(!$id?'1':$id->id+1).".png";
                    $path = storage_path('app/public/').$filename;
                    Image::make(file_get_contents($request->image))->save($path);
                }else{
                    $filename= null;
                }

                $user = User::create([
                    'name' => $name,
                    'email' => ($request->input('email') ? $request->input('email') : 'Anonymous'.((int)$last_id+1).'@covaid.abcsytems.com.ph' ),
                    'type' => 2,
                    'password' => Hash::make($request->input('email')),
                    'image' => $filename,
                ]);
                \auth()->login($user);
            }
            $donate = Donation::create([
                'user_id'       => $user->id,
                'amount'        => floatval(implode(explode(',',$request->input('amount')))),
                'comment'       => $request->input('comment'),
                'hide_amount'   => ($request->input('hide_amount') ? $request->input('hide_amount') : 0),
                'hide_info'     => ($request->input('hide_info') ? $request->input('hide_info') : 0),
                'hide_comment'  => ($request->input('hide_info') ? $request->input('hide_info') : 0)
            ]);

            $this->proofDonation($user->id,$request->attachment,$donate->id);

            //function for email
            // self::sendMail($user->email,$donate->id);
            $response = array(
                'status' => 'success',
                'reload' => true,
                'route' => route('donate.success',$donate->id),
                'desc'  => 'Account successfully created!'
            );

       

        return response()->json($response,200);
    }

    public function donationSuccess($id){
        $donation = Donation::findOrFail($id);
        return view('donate.success',['donation' => $donation]);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function sendMail($email,$id)
    {
        Mail::to($email)->queue(new Donated($id));
    }

    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function gcash()
    {
        return view('donate.gcash');
    }

    public function bank()
    {
        return view('donate.bank');
    }

    public function proofDonation($id, $attachment,$donate_id){
        if($attachment){
            $base64_str = substr($attachment, strpos($attachment, ",")+1);
            $image = base64_decode($base64_str);
            $filename = "proof-".time().($id).".png";
            $path = storage_path('app/public/').$filename;
            Image::make(file_get_contents($attachment))->save($path);

        }else{
            $filename= '';
        }

        $proof_query = new DonationProof([
            'donation_id'=> $donate_id,
            'attachment' => $filename
        ]);
        $proof_query->save();
    }

    public function showDonors()
    {
        $donors = Donation::whereHas('getProof',function (Builder $query ){
            $query->whereNotNull('verified_date');
        })
            ->selectRaw('SUM(amount) AS amount,user_id,created_at')
            ->groupBy('user_id','created_at')
            ->paginate(8);
//        dd($donors);
//        $donors = Donation::paginate(8);
        return view('donate.donors',['donors' => $donors]);
    }
}
