<?php
namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class CovidDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('covid_data.index',[
            'total_cases' => self::getCovidCase(1),
            'recovered' => self::getCovidCase(2),
            'death_count' => self::getCovidCase(3),
            'pui' => self::getCovidData('https://ncovph.digitaldeskph.com/api/total-puis'),
            'pum' => self::getCovidData('https://ncovph.digitaldeskph.com/api/total-pums'),
            'last_updated' => self::getTimeStamp(),
            'residences' => self::getCovidData('https://ncovph.digitaldeskph.com/api/cases-by-residence')
        ]);
    }

    public function getCovidData($url){
        $guzzle_client = new Client();
        $api_call = $guzzle_client->get($url);
        $call_arr =  json_decode($api_call->getBody(), true);
        return $call_arr['data'];
    }

    public function getTimeStamp(){
        $guzzle_client = new Client();
        $api_call = $guzzle_client->get('https://ncovph.digitaldeskph.com/api/total-puis');
        $call_arr =  json_decode($api_call->getBody(), true);
        return date("F d,Y H:i:s", $call_arr['time']);
    }

    public function getCovidCase($status){
        $data = 0;
        if($status == 1 || $status == 2){
            $url_ext = 'https://ncovph.digitaldeskph.com/api/'.($status==1?'total-confirmed':'total-recovered');
            $data = self::getCovidData($url_ext);
        }else if($status == 3){
            $data = self::getDeathCases();
        }
        return $data;
    }

    public function getDeathCases(){
        $guzzle_client = new Client();
        $deaths_api = $guzzle_client->get('https://api.covid19api.com/country/philippines/status/deaths/live');
        $deaths_arr = json_decode($deaths_api->getBody(), true);
        return $deaths_arr[count($deaths_arr)-1]['Cases'] ?? '';
    }

    public function getCaseTrends(){
        $trend_data = self::getCovidData('https://ncovph.digitaldeskph.com/api/confirmed-cases-trend');
        $admitted = array_values(array_column($trend_data,'admitted'));
    }

    public function getCaseByAge(){
        $age_data = self::getCovidData('https://ncovph.digitaldeskph.com/api/cases-by-age-group');
        $male_arr = array();
        $female_arr = array();
        $category_arr = array();
        $total_arr = array();
        foreach($age_data as $age){
            array_push($male_arr, $age['male']);
            array_push($female_arr, $age['female']);
            array_push($category_arr, $age['category']);
            $total = $age['male'] + $age['female'];
            array_push($total_arr, $total);
        }



        return array(
            "total" => $total_arr,
            "male" => array_values($male_arr),
            "female" => array_values($female_arr),
            "age_group" => $category_arr,
        );
    }

    public function getDeaths(){
        $guzzle_client = new Client();
        $deaths_api = $guzzle_client->get('https://api.covid19api.com/country/philippines/status/deaths/live');
        $deaths_arr = json_decode($deaths_api->getBody(), true);
        $death_dates = array_values(array_column($deaths_arr,'Date'));
        $death_counts = array_values(array_column($deaths_arr,'Cases'));
        $death_date_formatted = array();
        foreach ($death_dates as $death_date){
            array_push($death_date_formatted, date('M-d' , strtotime($death_date)));
        }
        return ['death_dates' => $death_date_formatted, 'death_counts'=>$death_counts, 'latest_count' => $deaths_arr[count($deaths_arr)-1]['Cases'] ?? ''];
    }

    public function getRecovered(){
        $guzzle_client = new Client();
        $recovered_api = $guzzle_client->get('https://api.covid19api.com/country/philippines/status/recovered/live');
        $recovered_arr = json_decode($recovered_api->getBody(), true);
        $recovered_dates = array_values(array_column($recovered_arr,'Date'));
        $recovered_counts = array_values(array_column($recovered_arr,'Cases'));
        $recovered_date_formatted = array();
        foreach ($recovered_dates as $recovered_date){
            array_push($recovered_date_formatted, date('M-d' , strtotime($recovered_date)));
        }
        return ['recovered_dates' => $recovered_date_formatted, 'recovered_counts' => $recovered_counts, 'latest_count' => $recovered_arr[count($recovered_arr)-1]['Cases'] ?? ''];
    }

    public function getConfirmed(){
        $guzzle_client = new Client();
        $confirmed_api = $guzzle_client->get('https://api.covid19api.com/country/philippines/status/confirmed/live');
        $confirmed_arr = json_decode($confirmed_api->getBody(), true);
        $confirmed_dates = array_values(array_column($confirmed_arr,'Date'));
        $confirmed_counts = array_values(array_column($confirmed_arr,'Cases'));
        $confirmed_date_formatted = array();
        foreach ($confirmed_dates as $confirmed_date){
            array_push($confirmed_date_formatted, date('M-d' , strtotime($confirmed_date)));
        }
        return ['confirmed_dates' => $confirmed_date_formatted, 'confirmed_counts'=>$confirmed_counts, 'latest_count' => $confirmed_arr[count($confirmed_arr)-1]['Cases'] ?? ''];
    }

    public function getCovidCasesChart(){


        return array(
            'deaths'=> self::getDeaths(),
            'confirmed'=> self::getConfirmed(),
            'recovered'=> self::getRecovered(),
            'age_chart' => self::getCaseByAge()
        );
    }
}
