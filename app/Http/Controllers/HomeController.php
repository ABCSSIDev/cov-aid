<?php

namespace App\Http\Controllers;

use App\DonationProof;
use App\Mail\Donated;
use DateTime;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Update;
use App\UpdateImage;
use NumberFormatter;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $updates = $this->getUpdates();
        $content = array();
        array_push($content,$this->PPEContent(),$this->PerishableContent(),$this->ReliefContent());
//        $covid = self::getCovidData('https://ncovph.digitaldeskph.com/api/cases-by-residence');
        // Mail::to('kennethwastaken13@gmail.com')->send(new Donated(1));
        return view('home',['projects' => $content, 'donation_sum' =>$this->getDonationSum(),'updates' => $updates]);
    }

    public function getUpdates(){
        $updates = Update::all();
        return $updates;
    }


    public function getImage($id){
        
            $image = UpdateImage::findOrFail($id);
            $file = Storage::disk('public')->get($image->image_url);
            return response($file,200);
    }


    public function PPEContent(){
        $ppe = array([
            'id' => "1",
            'image_thumb' => asset('images/PPE.jpg'),
            'title' => "Personal Protective Equipment Suit (PPE'S)",
            'subtitle' => "Our medical professionals are in dire need of Personal Protective Equipments. These PPEs are their only shield against our enemy – the coronavirus.",
            'content_html' => '<p>Many of our medical personnel, who are working round the clock in the hospitals are being infected by the disease as the number of patients grows by more than 500 each day. Their PPEs are unfortunately running low and without these it would be crucial for them to work and function effectively while avoiding being contracted with the virus. Hundreds of these medical practitioners are already quarantined and some have lost the battle.</p>
            <p>We at DECON Business Solution together with ABC Systems Solutions Inc. partnered up with local sewing businesses in Metro Manila to help us procure materials in order to make these PPEs and provide it to hospitals around the metro.</p>
            <p>Please see list of targeted hospitals:</p>
            <ol>
                <li>Ospital ng Taguig at Pateros</li></li>
                <li>South City Hospital and Medical Center (Daang Hari, Bacoor City)</li>
                <li>Research Institute of Tropical Medicine</li>
                <li>Las Piñas Doctors Hospital</li>
                <li>Las Piñas City Medical Center</li>
                <li>Philippine General Hospital</li>
                <li>Lung Center of the Philippines</li>
                <li>Gat Andres Bonifacio Memorial Medical Center</li>
                <li>UniHealth Parañaque Hospital and Medical Center</li>
                <li>Parañaque Doctors Medical Center</li>
                <li>Medical Center Taguig</li>
                <li>Diliman Doctors Hospital</li>
                <li>PNP General Hospital</li>
                <li>Rizal Provincial Hospital</li>
                <li>Binakayan Hospital and Medical Center (Kawit, Cavite)</li>
                <li>Carmona Hospital and Medical Center</li>
                <li>St. Frances Cabrini Hospital and Medical Center (Sto. Tomas, Batangas)</li>
                <li>Medical Center Muntinlupa</li>
                <li>Medical Center Imus</li>
                <li>Metropolitan Medical Center</li>
                <li>Paredes Primary Care Center</li>
                <li>The Premier Medical Center</li>
                <li>Veterans Memorial Medical Center</li>
                <li>Divine Grace Medical Center</li>
                <li>Tarlac Public Hospital</li>
                <li>Sta. Ana Hospital Manila</li>
                <li>National Kidney Transplant Institute</li>
                <li>Ospital ng Muntinlupa</li>
                <li>San Juan De Dios Hospital</li>
                <li>Chinese General Hospital</li>
                <li>Mary Johnston Hospital</li>
                <li>Manila Adventist Medical Center</li>
          </ol>
          <p>Once we’re done with these hospitals we’ll expand to the ones outside of Metro Manila. </p>
          <p>Apart from that, your donations will be used to purchase medical-grade surgical masks, N95 masks, goggles, safety glasses and face shields.</p>
          <p>Let us help our medical frontliners procure these items! Together we can stand and fight this deadly virus. Together WE WILL RISE AS ONE!</p>
          <p>Every peso counts! CLICK ON THE BUTTON TO DONATE!</p>',
        ]);

        return $ppe;
    }

    public function ReliefContent(){
        $relief = array([
            'id' => "2",
            'image_thumb' => asset('images/Relief_Goods.jpg'),
            'title' => "Food Packs and Grocery Items",
            'subtitle' => "always been a vital part in helping those people who have scarce supplies (food, water, clothes, etc.) in times of natural or man-made disasters.",
            'content_html' => '
            <p><b>Food Packs</b> - Health care workers are working round the clock as the number of patients increases. Police and military personnel are working extensively in ensuring peace and order in the community as we go through this enhanced community quarantine. Delivery riders are brazing the summer heat just to make sure our foods are delivered clean and safe,</p>
            <p>Though our primary concern is to provide our frontline workers the PPEs that they need, we would like to take a portion of your donation to provide a meal twice a week for them to enjoy. A home cooked meal would surely help them boost their energy to do their daily tasks. This home cooked meals will be prepared at ABCSSI’s headquarters located in Mandaluyong City.</p>
            <p><b>Grocery Items</b></p><hr>
            <p>Many of our brethren are living in a daily income. A day job earns them a salary and brings food on their table. Many rely on their daily income by being a jeepney, tricycle or pedicab drivers. Some are selling sampaguita, bottled water and candy along the streets. Others gamble their chances to sell something along the busy streets of Metro Manila.
            Due to month-long community quarantine where no one can go out, their livelihood has stopped. They are now relying on the mercy of the relief goods being distributed by the local government units. However, this is not enough.
            We would like to become a medium to help our brethren who are less fortunate. A food and grocery items for the homeless and those unprivileged. We wanted to partner with you to help these brethren have at least food on their table.
            A portion of your donation will be used to buy groceries for them because we believe that NO ONE SHALL BE HUNGRY in times of pandemic.
            </p>',
        ]);

        return $relief;
    }

    public function PerishableContent(){
        $perishable = array([
            'id' => "3",
            'image_thumb' => asset('images/Perishable_Goods.jpg'),
            'title' => "Logistics for Perishable Goods",
            'subtitle' => "We create memories through our childhood experiences. We experienced being punished by our parents because they love us. ",
            'content_html' => '<p>We wake up every morning with a warm embrace of our mothers. We also have siblings to play and fight with. However, there are numbers of children without families placed in a custody of caretakers. We called it orphanage,</p>
            <p>It is pleasant to the eyes seeing our grandparents play and being protective with their grandchildren. However, there are handful of them brought in a house with their co-senior citizens and being helped by kind-hearted home for the aged personnel. They are more prone and have higher risk in acquiring the corona virus.
            Let us work together and bring healthy vegetables to these home for the aged and orphanage around Metro Manila. We will work closely with our farmers by buying their vegetables and distribute it to our brethren who are in the Home for the Aged and orphanages.
            </p>',
        ]);

        return $perishable;
    }
    public function getCovidData($url){
        $guzzle_client = new Client();
        $api_call = $guzzle_client->get($url);
        $call_arr =  json_decode($api_call->getBody(), true);
        return $call_arr['data'];

       Mail::to('kennethwastaken13@gmail.com')->send(new Donated(1));
        return view('home', ['donation_sum'=>self::getDonationSum()]);
    }

    public function getDonationSum(){
        $total = 0;
        $proofs = DonationProof::whereNotNull('verified_by')->get();
        foreach ($proofs as $proof){
            $total += $proof->donorData->amount;
        }
        return "₱".number_format($total, 2, '.', ',') ;
    }

}
