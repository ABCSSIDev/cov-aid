<?php

namespace App\Http\Controllers\Admin;

use App\Donation;
use App\DonationProof;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Contracts\DataTable;
use Yajra\DataTables\Facades\DataTables;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class DonationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('is_admin');
    }

    public function index()
    {
        return view('admin.donation.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.donation.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->file('attachment2'));
//        dd($request->input());
        try{
            foreach ($request->input('for_insert') as $value){
//            insert user
                $user = new User();
                $user->name = ($request->input('donor'.$value) ? $request->input('donor'.$value) : 'Anonymous');
                $user->email = 'Anonymous'.User::getUserID().'@covaid.abcsytems.com.ph';
                $user->password = md5(rand(1,10000));
                $user->type = 2;
                $user->save();

//            insert donation
                $donation = new Donation();
                $donation->user_id = $user->id;
                $donation->amount = floatval(implode(explode(',',$request->input('amount'.$value))));
                $donation->save();

//            insert proof
                if($request->has('attachment'.$value)){
                    $base64_str = substr($request->file('attachment'.$value), strpos($request->file('attachment'.$value), ",")+1);
                    $image = base64_decode($base64_str);
                    $filename = "proof-".time().($donation->id).".png";
                    $path = storage_path('app/public/').$filename;
                    Image::make(file_get_contents($request->file('attachment'.$value)))->save($path);

                }else{
                    $filename= '';
                }
                $donationProof = new DonationProof();
                $donationProof->donation_id = $donation->id;
                $donationProof->attachment = $filename;
                $donationProof->donation_method = $request->input('donation_send'.$value);
                $donationProof->verified_by = Auth::user()->id;
                $donationProof->verified_date = today();
                $donationProof->save();
            }
            $response = array(
                'status' => 'success',
                'desc'  => 'Donations successfully created!'
            );
        }catch (\Exception $exception){
            $response = array(
                'status' => 'error',
                'desc'  => $exception->getMessage()
            );
        }
        return response()->json($response,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $donor_details = DonationProof::where('id', $id)->first();
        return view('admin.donation.show',['donor_details' => $donor_details]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $proof_donation = DonationProof::findOrFail($id);
            $proof = DonationProof::where('id', $id)->update([
                'verified_by' => Auth::User()->id,
                'verified_date' => date('y-m-d',strtotime($request->input('date_verified'))),
                'donation_method' => $request->input('donation_send'),
            ]);
            $donate = Donation::where('id', $proof_donation->donorData->id)->update([
                'amount' => floatval(implode(explode(',',$request->input('verified_amount'))))
            ]);
            $response = array(
                'status' => 'success',
                'desc'  => 'Donations successfully created!',
                'reload' => true,
                'route' => null
            );
        }catch (\Exception $exception){
            $response = array(
                'status' => 'error',
                'desc'  => $exception->getMessage()
            );
        }
        return response()->json($response,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function donationList(){
        $donations = Donation::all();
        return DataTables::of($donations)->addColumn('donation_id',function($model){
            return $model->id;
        })->addColumn('donor',function($model){
            return $model->userData->name;
        })->addColumn('amount',function($model){
            return number_format($model->amount,2);
        })->addColumn('date_donated',function($model){
            return date("F d, Y", strtotime($model->created_at));
        })->addColumn('status',function($model){
            return (is_null($model->getProof->verified_date) ? "For Verification" : "Verified") ;
        })->addColumn('action',function($model){
            return '<center>
                        <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                            <i class="fa fa-bars"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="'.route('donation.show',$model->getProof->id).'"><i class="fa fa-eye" aria-hidden="true"></i> View Record</a>
                            '.(is_null($model->getProof->verified_date) ? '<a class="dropdown-item popup_form" data-toggle="modal" title="'.$model->userData->name.' - ₱'.number_format($model->amount,2).'" href="" data-url="'.route('verify.show',$model->getProof->id).'" ><i class="fa fa-check" aria-hidden="true"></i> Verify Donation</a>' : "").'
                            <a class="dropdown-item" href=""><i class="fa fa-pencil" aria-hidden="true"></i> Edit Record</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item popup_form" data-toggle="modal" style="cursor:pointer" data-url="" title="Delete Invoice Record"><i class="fa fa-trash" aria-hidden="true"></i> Delete Record</a>
                        </div>
                    </div>
                    </center>';
        })->escapeColumns([])
            ->make(true);
    }

    public function getImage($id){
        try{
            $proof = DonationProof::findOrFail($id);
            $file = Storage::disk('public')->get($proof->attachment);
            return response($file,200);
        }catch (Exception $exception){
            $response = Common::catchError($exception);
        }
    }

    public function verifyDonation($id){
        $proof = DonationProof::findOrFail($id);
        return view('admin.donation.verification', ['id' => $id,'amount' => $proof->donorData->amount]);
    }
}
