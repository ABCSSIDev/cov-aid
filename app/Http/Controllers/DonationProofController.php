<?php

namespace App\Http\Controllers;

use App\DonationProof;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class DonationProofController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('proof_donation.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->input('id');
        if($request->has('attachment')){
            $base64_str = substr($request->attachment, strpos($request->attachment, ",")+1);
            $image = base64_decode($base64_str);
            $filename = "proof-".time().($id).".png";
            $path = storage_path('app/public/').$filename;
            Image::make(file_get_contents($request->attachment))->save($path);

        }else{
            $filename= '';
        }

        $donation = DonationProof::where('donation_id', $id)->first();
        if($donation){
            $proof_query = DonationProof::where('donation_id', $id)->update([
                'donation_id'=> $id,
                'attachment' => $filename
            ]);
        }else{
            $proof_query = new DonationProof([
                'donation_id'=> $id,
                'attachment' => $filename
            ]);
            $proof_query->save();
        }



        $response = array(
            'status' => 'success',
            'desc'  => 'Account successfully created!'
        );


        return redirect()->route('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('proof_donation.create', ['id'=>$id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
