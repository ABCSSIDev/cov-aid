<?php

namespace App\Http\Controllers;

use App\FacebookAccount;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use Laravel\Socialite\Contracts\User as ProviderUser;

class FacebookController extends Controller
{
    //

    public function redirectToFacebook(){
        return Socialite::driver('facebook')->redirect();
    }

    public function handleCallback(){
        $user = self::createOrGetUser(Socialite::driver('facebook')->user());
        \auth()->login($user);
        return redirect()->to('home');
//        try {
//            $user = Socialite::driver('facebook')->user();
//            $userModel = new User();
//            $userModel->facebook_id = $user->getId();
//            $userModel->name = $user->getName();
//            $userModel->email = $user->getEmail();
//            $userModel->image = $user->getAvatar();
////            $userModel->password = $user
//            $userModel->save();
////
//            Auth::loginUsingId($userModel->id);
////
////
//            return redirect()->route('home');
//
//
//        } catch (\Exception $e) {
//
//dd($e->getMessage());
//            return redirect('auth/facebook');
//
//
//        }
    }

    public function createOrGetUser(ProviderUser $providerUser){
        $account = FacebookAccount::whereProvider('facebook')
            ->whereProviderUserId($providerUser->getId())
            ->first();
        if ($account){
            return $account->user;
        }else{
            $facebook = new FacebookAccount();
            $facebook->provider_user_id = $providerUser->getId();
            $facebook->provider = 'facebook';

            $user = User::whereEmail($providerUser->getEmail())->first();

            if (!$user){
                $user = new User();
                $user->email = $providerUser->getEmail();
                $user->name = $providerUser->getName();
                $user->password = md5(rand(1,10000));
                $user->type = 2;
                $user->image = $providerUser->getAvatar();
                $user->save();
            }

            $facebook->user()->associate($user);
            $facebook->save();

            return $user;
        }
    }
}
