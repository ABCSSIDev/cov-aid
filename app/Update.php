<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\UpdateImage;

class Update extends Model
{
    use SoftDeletes;
    protected $primaryKey = 'id';

    protected $fillable = ['project_id', 'title', 'description','date_posted','created_by'];
    protected $dates = ['deleted_at'];

    public function updateDetails(){
       return $this->hasMany('App\UpdateImage','update_id');
   }

   public function getFirstImage(){
        return UpdateImage::orderBy('sort_order','ASC')->first()->updateDetails();
   }

}
