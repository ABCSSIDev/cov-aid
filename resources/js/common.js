/**
 * function to load print form to designated html file
 */
$(document).on('click','.print-form',function (e) {
    e.preventDefault();
    let form_id = $(this).attr('data-form');
    $('#'+form_id).removeAttr('hidden');
    printJS({
        printable: form_id,
        type: 'html',
        showModal: true,
        scanStyles:false,
        css: '/css/print.css'
    })
    $('#'+form_id).attr('hidden', true);
    return false;
});

/**
 * function to load for common scripts
 */
$(function () {
    //function to load bootstrap datetimepicker
    $('.datepicker').datetimepicker({
        format: 'L'
    });
    $('.timepicker').datetimepicker({
        format: 'LT'
    });

    //function to load jquery.number mask
    // $(".amount").number(true);

    //script to load bootstrap selectpicker
    $('.selectpicker').selectpicker();

});
$(document).on('focus','.amount',function () {
    $(this).number(true);
})
//function for call for modal layout
$(document).on('click','.popup_form',function(e){
    e.preventDefault();
    var title = $(this).attr('title');
    var url = $(this).attr('data-url');
    $('.modal-title').html(title);
    $.ajax({
        method: "GET",
        url: url,
        success: function(data){
            $('#myModal').modal({backdrop:'static'});
            $('#myModal').modal('show');
            $('div.modal-body').html(data);
        }
    });
});



