(function($) {
    //general function for validation and submit of forms
    $.fn.registerFields = function (rules, resets=true) {
        let $form_id = $(this);
        $($form_id).validate({
            rules: rules,
            errorPlacement: function(error,element){//custom function for special cases of the error messages
                let placement = $(element).data('error');
                if(placement){
                    $(placement).append(error)
                }else{
                    error.insertAfter(element);
                }
            },
            highlight: function (element) {
                $(element).parent().addClass('validate-has-error');
            },
            unhighlight: function (element) {
                $(element).parent().removeClass('validate-has-error');
            },
            submitHandler: function (event) {
                let $formData = new FormData($form_id[0]);
                $.ajax({
                    type: 'POST',
                    url: $form_id.attr('action'),
                    data: $formData,
                    dataType: 'json',
                    async: false,
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        //add pre load animation
                       $('#spinner-grow-in').removeAttr('hidden');
                    },
                    error: function (data) {
                        toastMessage('Ooops!','data.desc,data.status');
                        $('#spinner-grow-in').fadeOut('slow', function(){
                            $('#spinner-grow-in').attr('hidden');
                        });
                    },
                    success: function (data) {
                        setTimeout(function(){
                            $('#spinner-grow-in').fadeOut('slow', function(){
                                $('#spinner-grow-in').attr('hidden');
                            });
                          }, 5000);
                        if (data.status === "success"){
                            if (data.reload){
                                if(data.route == null){
                                    window.location.reload();
                                }
                                else{
                                    window.location.href = data.route;
                                } 
                            }
                            if(resets){
                                $form_id.each(function(){
                                    this.reset(); //resets the form values
                                });
                            }
                            $('.remove_row').remove()
                            if(data.reload_table){
                                $.ajax({
                                    url: data.route,
                                    method: "GET",
                                    success: function(datas){
                                        $("#"+data.tableid+"").empty(); 
                                        $("#"+data.tableid+"").html(datas);

                                        //delete only append data
                                        $(".appendRow").remove();

                                        $("#image").attr("src","{{asset('images/default.png')}}");
                                        //this function uses for reloading data needed
                                        reloadData(datas);
                                    },
                                    
                                });
                            }
                            toastMessage('Success',data.desc,data.status);

                        } else{
                            toastMessage('Ooops!',data.desc,data.status);
                        }
                    },
                    complete: function () {
                        
                    }
                });
                return false;
            },
            invalidHandler: function(event, validator){
                toastMessage('Oopps!','It seems like you missed some required fields',"error");
            }
        });
    };

}(jQuery));


//Show Toastr Function
function toastMessage(title,message,toastType){
    toastr.remove();
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    toastr[toastType](message, title);
}

// $(document).ready(function() {
//     //Toast alert after deleting
//     var url_string = window.location.href
//     var url = new URL(url_string);
//     var param = url.searchParams.get("success");
//     var param2 = url.searchParams.get("module");
    
//     console.log(param);
//     if(param == 'true'){
//         //toastMessage('Success',param2+' successfully Verified!',"success");
//         window.history.pushState({}, document.title, "/" + url.searchParams.get("url")); //remove url param
//         //$('#spinner-grow-in').attr('hidden',true);
//     }
// });