
{{--<div>--}}
    {{--<h1 id="confirmed_cases">Confirmed Cases: </h1>--}}
    {{--<h2 id="recovered_cases">Recovered Cases: </h2>--}}
    {{--<h3 id="deaths">Death Cases: </h3>--}}
{{--</div>--}}
{{--<div class="col-12 text-center" style="margin-top:40px;margin-bottom:40px;">--}}
    {{--<div class="text-center">--}}
        {{--<h3 class="tracker-title">PHILIPPINES COVID-19 TRACKER</h3>--}}
        {{--<p>Be updated on the spread of <strong>COVID-19</strong> in our country</p>--}}
    {{--</div>--}}
    {{--<div class="cases">--}}
        {{--<div class="row text-center">--}}
            {{--<div class="col-3"></div>--}}
            {{--<div class="col-2 case-item is-red">--}}
                {{--<h6 class="cases-title">Confirmed</h6>--}}
                {{--<h2 class="cases-content" id="confirmed_cases">0</h2>--}}
            {{--</div>--}}
            {{--<div class="col-2 case-item is-green">--}}
                {{--<h6 class="cases-title" >Recovered</h6>--}}
                {{--<h2 class="cases-content" id="recovered_cases">0</h2>--}}
            {{--</div>--}}
            {{--<div class="col-2 case-item is-black">--}}
                {{--<h6 class="cases-title">Deceased</h6>--}}
                {{--<h2 class="cases-content" id="deaths">0</h2>--}}
            {{--</div>--}}
            {{--<div class="col-2"></div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<div>--}}
        {{--<a class="button-medium" href="{{route('covid_data.index')}}">See More Statistics</a>--}}
    {{--</div>--}}

{{--</div>--}}

{{--<div>--}}
{{--<h1 id="confirmed_cases">Confirmed Cases: </h1>--}}
{{--<h2 id="recovered_cases">Recovered Cases: </h2>--}}
{{--<h3 id="deaths">Death Cases: </h3>--}}
{{--</div>--}}
<div class="col-12 text-center">

    <div class="neumorphic-card mb-5" style="padding: 3em 0">
        <h3 class="h3-dashboard">COVID-19 CORONAVIRUS PANDEMIC</h3>
        <p>Be updated on the spread of <strong>COVID-19</strong> in our country</p>
        <hr>
        <div class="row">
            <div class="col-sm-1 mx-auto">
            </div>
            <div class="col-sm-3 mx-auto">
                <div class=" tile-stats tile-blue">
                    <div class="icon">
                        <i class="fav fa-receipt"></i>
                    </div>
                    <div class="num or-count" id="confirmed_cases" data-num="">0</div>
                    <h3><strong>COVID-19 Cases</strong></h3>
                </div>
            </div>
            <div class="col-sm-3 mx-auto">
                <div class=" tile-stats tile-green">
                    <div class="icon">
                        <i class="fav fa-donate"></i>
                    </div>
                    <div class="num payable" id="recovered_cases" data-num="">0</div>
                    <h3><strong>Recovered Cases</strong></h3>
                    <p></p>
                </div>
            </div>
            <div class="col-sm-3 mx-auto">
                <div class="tile-stats tile-red">
                    <div class="icon">
                        <i class="fav fa-hand-holding-usd"></i>
                    </div>
                    <div class="num receivable" id="deaths" data-num="">0</div>
                    <h3><strong>Deaths</strong></h3>
                    <p></p>

                </div>
            </div>
            <div class="col-sm-1 mx-auto">
            </div>
        </div>
        <div class="col-12 text-center" style="margin-top: 15px">
            <a href="{{route('covid_data.index')}}"> >>See More Statistics<< </a>
        </div>
        
        {{--<div class="col-12" align="center">--}}
        {{--<canvas id="myChart" width="1000" height="300"></canvas>--}}
        {{--</div>--}}
    </div>
</div>
@push('scripts')
    <script>

        var jsonData = $.ajax({
            url: '{{ route("covid.cases") }}',
            dataType: 'json'
        }).done(function(results){
           $('#confirmed_cases').html(results.confirmed.latest_count);
            $('#recovered_cases').html(results.recovered.latest_count);
            $('#deaths').html( results.deaths.latest_count);
        });

    </script>
@endpush

<style>
    .validate-has-error {
        border-color: #cc2424;
    }
    .validate-has-error:focus {
        border-color: #cc2424;
    }
    .validate-has-error + label.validate-has-error,
    .validate-has-error > label.validate-has-error,
    .input-group + .validate-has-error {
        color: #cc2424;
        display: inline-block;
        margin-top: 5px;
    }
    .validate-has-error .validate-has-error {
        color: #cc2424;
        display: inline-block;
        margin-top: 5px;
    }
    .make-switch + .validate-has-error {
        margin-left: 10px;
    }
    .validate-has-error .form-control {
        border-color: #cc2424;
    }
    .validate-has-error .error {
        color: #cc2424;
    }
    .has-error{
        border-color: #cc2424 !important;
    }
    //General CSS
      .breadcrumb{
          margin-top:24px;
          background-color: white !important;
          margin-left: -16px;
      }
    .btn-primary{
        background-color: #00007c !important;
    }
    .form-head{
        margin-top: -15px;
        color: #00007c;
        font-weight: 600;
    }

    .tile-stats{
        position: relative;
        display: block;
        background: #303641;
        padding: 20px;
        margin-bottom: 10px;
        overflow: hidden;
        -webkit-border-radius: 5px;
        -webkit-background-clip: padding-box;
        -moz-border-radius: 5px;
        -moz-background-clip: padding;
        border-radius: 5px;
        background-clip: padding-box;
        -webkit-transition: all 300ms ease-in-out;
        -moz-transition: all 300ms ease-in-out;
        -o-transition: all 300ms ease-in-out;
        transition: all 300ms ease-in-out
    }
    //Autocomplete CSS
$ui-autocomplete-clear-size:  16px;

    .ui-autocomplete-input-has-clear {
        padding-right: $ui-autocomplete-clear-size * 1.5;
    }

    .ui-autocomplete-input-has-clear::-ms-clear {
        display: none;
    }

    .ui-autocomplete-clear {
        display: inline-block;
        width: $ui-autocomplete-clear-size;
        height: $ui-autocomplete-clear-size;
        text-align: center;
        cursor: pointer;
    }
    .tile-stats .num{
        font-size: 29px;
        font-weight: bold;
    }
    .tile-stats h3{
        font-size: 18px;
        margin-top: 5px;
    }
    .tile-stats p{
        font-size: 11px;
        margin-top: 5px;
    }
    .tile-stats div, .tile-stats h3, .tile-stats p{
        position: relative;
        color: #fff;
        z-index: 5;
        margin: 0;
        padding: 0;
    }
    .tile-stats.tile-red{
        background: #f56954;
    }
    .tile-stats.tile-green{
        background: #00a65a;
    }
    .tile-stats.tile-aqua{
        background: #00c0ef;
    }
    .tile-stats.tile-blue{
        background:#0073b7;
    }
    .tile-stats .icon i{
        font-size: 100px;
        line-height: 0;
        margin: 0;
        margin-bottom: 50px;
        padding: 0;
        vertical-align: bottom;
    }
    .tile-stats .icon {
        color: rgba(0, 0, 0, 0.1);
        position: absolute;
        right: 5px;
        bottom: 5px;
        z-index: 1;
    }
    .tile-stats .icon i:before {
        margin: 0;
        padding: 0;
        line-height: 0;
    }
    .neumorphic-card {
        margin-left: auto;
        margin-right: auto;
        padding: 10px;
        /* margin-top: 100px; */
        border-radius: 10px;
        /* width: 150px; */
        /* height: 150px; */
        background-color: white;
    // box-shadow: 9px 9px 16px rgb(163,177,198,0.6), -9px -9px 16px rgba(163, 177, 198, 0.6);
    }

    .thead-assign-inventory {
        background-color: #323739;
        color: #fff;
    }
    .h3-dashboard {
        font-weight: 600;
        margin-top: 5px;
    }
    .tile-stats.tile-PO {
        padding: 50px 15px;
        margin-bottom: 29px;
        margin-top: 29px;
    }
    .ms-container .ms-selection {
        float: none !important;
    }
    .ms-container .ms-selectable, .ms-container .ms-selection {
        width: 62%;
        margin-bottom: 2em;
    }
    .upload-signatur:focus {
        box-shadow: 0 0 0 0rem rgba(52, 144, 220, 0.25);
    }

    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    /* Firefox */
    input[type=number] {
        -moz-appearance:textfield;
    }

    a:not([href]) {
        cursor: pointer;
    }

    $dark : #000;

    //print
      .print-budget-allocation{
    .pad{
        padding: 20px 0 10px 0;
    }
    .margin-b{
        margin-bottom: 50px;
    }
    .margin-t{
        margin-top: 30px;
    }
    th,td{
        border: 1px solid $dark;
    }
    ol.basic{
        margin-bottom: 0px !important;
    }
    }

    .error{
        color: #cc2424;
    }

    @media screen  and (min-width : 100px) {
        .table-mediascreen{
            width: 100%!important;
        }
    }

    .right-align{
        text-align:right;
    }

    .v-align {
        vertical-align: middle !important;
    }

    //add border bootstrap select
      .bootstrap-select > .dropdown-toggle {
          border: 1px solid #ced4da !important;
      }
    .btn-light{
        background-color: #ffffff;
    }

    span.overdue-date{
        color: #e34142;
    }
    .details{
        height: 100%;
    }
    .bank-money{
        margin: auto;
        width: 50%;
        text-align: center;
    }
    .bank-money p{
        padding: 22px 0 7px;
        color: #454545;
        font-size: 19px;
        font-family: 'OpenSans-Regular';
        font-weight: normal;
        line-height: 1;
        margin: 0;
    }
    .bank-money .money-value{
        color: #789e4e;
        font-size: 45px;
        font-family: 'OpenSans-Semibold';
        font-weight: normal;
        line-height: 1;
    }
    .details button{
        width: 100%;
        margin-top: 14px;
    }
    .popover{
        max-width: 400px !important;
    }
    .popover-header{
        display:none;
    }
    .popover-container{
        padding: 16px;
        width: 400px;
    }
    .popover-container .section{
        width: 50%;
        float: left;
    }
    .popover-container .section h4{
        color: #789e4e;
        margin-bottom: 0px;
    //font-size: 15px;
        font-family: 'OpenSans-Semibold';
        font-weight: normal;
        padding: 14px 12px 2px;
        text-transform: capitalize;
        text-align: left;
    }
    .popover-container .section ul{
        list-style: none;
        padding: 9px 15px 0;
    }
    .popover-container .section ul li{
        float: none;
        display: block;
        padding: 9px 0;
        margin: 0 !important;
        font-size: 14px;
    }
    .popover-container .section ul li a{
        text-decoration: none;
        color: #000;
    }
    .popover-container .section ul li a:hover{
        color: #789e4e;
    }
    .card-table thead{
        background-color: #f9fbfd;
        text-transform: uppercase;
        font-size: .625rem;
        font-weight: 600;
        letter-spacing: .08em;
        color: #95aac9;
        border-bottom-width: 1px;
    }
    .card-table th{
        padding: 1rem;
    }
    .card-table td{
        padding: 1rem;
    }
    .card-table tr{
        cursor: pointer;
        text-transform: uppercase;
        font-size: .750rem;
        font-weight: 600;
        letter-spacing: .08em;
        border-bottom-width: 1px;
    }

    @media print {
        .page-break {
            clear: both;
            page-break-after: always;
        }
    }

    .login-cubes
    {
    .cube
    {
        position: absolute;
        height: 100px;
        width: 100px;
        margin: 0;
        animation: cube-fade-in 2s cubic-bezier(.165, .84, .44, 1);
        will-change: transform;

    @keyframes cube-fade-in
    {
        0%
        {
            opacity: 0;
            transform: scale(.5)
        }
    }

    *
    {
        position: absolute;
        height: 100%;
        width: 100%;
    }

    .shadow
    {
        background: #07427a;
        top: 40%;
    }

    .sides
    {
        transform-style: preserve-3d;
        perspective: 600px;

    div
    {
        backface-visibility: hidden;
        will-change: transform;
    }

    .front
    {
        transform: rotateY(0deg) translateZ(50px);
    }
    .back
    {
        transform: rotateY(-180deg) translateZ(50px);
    }
    .left
    {
        transform: rotateY(-90deg) translateZ(50px);
    }
    .right
    {
        transform: rotateY(90deg) translateZ(50px);
    }
    .top
    {
        transform: rotateX(90deg) translateZ(50px);
    }
    .bottom
    {
        transform: rotateX(-90deg) translateZ(50px);
    }
    }
    }
    }


    #wrapper {
        height:100%;
        width:100%;
        text-align: center;
        display: table;
        position:absolute;
    }

    #title {
        display: table-cell;
        vertical-align: middle;
        z-index: 999;
    }

    #title h2 {
        color: #fff;
        font-size: 45px;
        font-family: "museo-slab";
    }

    #title h3 {
        color: #fff;
        font-size: 25px;
        font-family: "museo-sans";
        font-weight: 300
    }


    #wrapper canvas {
        position: absolute;
        top: 0;
        left: 0;
        width: 1950px;
        height: auto;
    }

    #canvas {
        z-index: 1;
    }
    #canvasbg {
        z-index: -10;
        -webkit-filter: blur(3px);
        -moz-filter: blur(3px);
        -o-filter: blur(3px);
        filter: blur(3px);
        opacity: 0.6;

    }

    //REPORTS CARD DESIGN//

    $background:#4FA584;

    .card-reports{
    .ui{
    // width:1000px;
        margin:0 auto;
        font-family: 'Raleway', sans-serif;
        color:white;
        box-shadow:none;
        box-shadow:0px 0px 5px 0px 2e2e2e;
    ul{
        margin:0px 30px 10px 0px;
        padding:0;
        list-style-type:none;
        font-size:11px;
        font-weight:400;
        line-height:20px;
    }
    .drop{
        z-index:-999;
        opacity:0;
        width: 15rem;
        height: 50px;
        background: darken($background,10%);
        position: absolute;
        color:white;
        bottom: 0;
        padding:12px 30px 21px 30px;
        transition: all ease-in-out 0.3s;
        transition-property:bottom,opacity;
        transition-duration:.3s;
    p{
        color:lighten($background,50%);
        font-size: 14px;
    &:hover{
         text-decoration: underline;
     }
    }
    }
    &_box.white{
         background: #41678a;
         border: solid 1px;
         border-radius: 5px;
         color:#fff;
         box-shadow:-1px 0px rgba(0, 0, 0, 0.07);
         margin-left: 1rem;
     }
    &_box{
         width:15rem;
         height:120px;
         position:relative;
         background:#3d3d3d;
         margin-bottom:50px;
         float:left;
         box-shadow:-1px 0px rgba(255, 255, 255, 0.07);
         cursor:pointer;
         transform:scale(1);
         transition-property:transform,background;
         transition-duration:.3s;


    &__inner{
         padding:20px;

    span{
        font-size:36px;
        font-weight:700;
    }

    }
    h2{
        font-weight:normal;
        font-size:16px;
        margin:-4px 0px 3px 0px;
    &.txt-ellipsis{
         overflow: hidden;
         display: -webkit-box;
         -webkit-line-clamp: 2;
         -webkit-box-orient: vertical;
     }
    }
    p{
        font-size:11px;
        color:rgb(182, 182, 182);clear: left;
        font-weight:300;
        width:160px;
    }
    &:hover{
         background:#3d3d3d!important;
         color:#fff!important;
         transition-property:transform,background;
         transition-duration:.3s;
         position:relative;
         z-index:1;
    .txt-ellipsis{
        -webkit-line-clamp: 4;
    }
    }
    .icon{
        color: rgba(0, 0, 0, 0.1);
        position: absolute;
        right: 5px;
        bottom: 5px;
        z-index: -1;
        font-size: 100px;
    }
    }
    }


    .ui_box:hover > .ui_box__inner p{
        color:lighten($background,30%);
    }
    .ui_box:hover > .drop{
        transition-property:bottom,opacity;
        transition-duration:.3s;
        bottom:-42px;
        z-index:-99999999;
        opacity:1;
    }
    .ui_box:hover > .drop .arrow{
        transition-property:transform;
        transition-duration:1s;
        transform:rotate(765deg);
    }
    .ui_box:hover > .ui_box__inner .progress_graph > div{
        background:white;
    }
    .ui_box:hover > .ui_box__inner .progress .progress_bar,.ui_box:hover > .ui_box__inner .progress .progress_bar--two{
        background:white;
    }
    .stat_left{float:left;}
    .arrow{
        width: 4px;
        height: 4px;
        transition-property:transform;
        transition-duration:1s;
        transform:rotate(45deg);
        -webkit-transition-timing-function: cubic-bezier(0.68, -0.55, 0.265, 1.55);
        border-top: 1px solid #CDEAD3;
        border-right: 1px solid #CDEAD3;
        float: right;
        position: relative;
        top: -37px;
        right: 0px;

    }

    @keyframes bar{
        from{width:0px}
        to{width:58%;}
    }

    @keyframes bar2{
        from{width:0px}
        to{width:78%;}
    }

    @keyframes graph{
        from{height:0px}
        to{height:20px}
    }
    @keyframes graph2{
        from{height:0px}
        to{height:30px}
    }
    @keyframes graph3{
        from{height:0px}
        to{height:24px}
    }
    @keyframes graph4{
        from{height:0px}
        to{height:13px}
    }
    }

    //END OF CARD DESIGN IN REPORTS//

    // table
       .tab_header {
           background-color: #323739 !important;
           color: #fff !important;
           text-align: center !important;
       }
    .table-bordered {
        border: 1px solid #dee2e6;
    }
    table.dataTable {
        clear: both;
        margin-top: 6px !important;
        margin-bottom: 6px !important;
        max-width: none !important;
        border-collapse: separate !important;
        border-spacing: 0;
    }
    .table-bordered th, .table-bordered td {
        border: 1px solid #dee2e6;
    }
    .table-sm td {
        padding: 1rem!important;
    }
    .table-sm th{
        padding: 0.3rem!important;
    }
    thead tr th {
        font-weight: 500;
    }
    .table-sm th.pt2 {
        padding: 0.5rem!important;
    }
    .fw-700 {
        font-weight: 700;
    }
    .align-left {
        text-align: left !important;
    }
    .align-right {
        text-align: right;
    }
    .mb-50 {
        margin-bottom: 50px;
    }
    .mt-2r{
        margin-top: 2rem;
    }
    .mt-02 {
        margin-top: 2rem;
    }
    .mt-50 {
        margin-top: 50px;
    }
    .mg-left-9 {
        margin-left: 9rem;
    }
    .mg-left-10 {
        margin-left: 10rem;
    }
    .mg-left-12 {
        margin-left: 12rem;
    }
    .hr1{
        border: 1!important;
    }
    .p-truly {
        margin-left: 29rem;
        position: relative;
        padding: 1rem;
    }
    .p-inspect {
        margin-left: 5rem;
        margin-top: -1px;
    }
    .p-pb01 {
        margin-left: 35rem;
        position: relative;
        padding-bottom: 1rem;
    }
    .m-position {
        margin-left: 9rem;
        margin-top: -1px;
    }
    .p-position-name {
        position: relative;
        top: 13px;
    }
    .indent-2 {
        text-indent: 2rem;
    }

    //Purchase Request
      .input-total-pr {
          border: none;
          background-color: transparent;
          width: 180px;
          text-align: right;
      }
    .input-total-pr:focus {
        outline: -webkit-focus-ring-color auto 0px;
        outline-color: -webkit-focus-ring-color;
        outline-style: auto;
        outline-width: 0px;
    }
    .total-cost{
        font-size: 1.5rem;
    }
    .table-sm{
    .state-app{
    td{
        padding: 0.3rem !important;
    }
    }
    }
    .search-table-outter > table {
        display: block;
        overflow-x: auto;
        white-space: nowrap;
    }
    .no-wrap {
        white-space: nowrap;
    }
    .mr-n26{
        margin-right: -26px;
    }
</style>
