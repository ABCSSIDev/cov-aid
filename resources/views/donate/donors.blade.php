@extends('layouts.app')

@section('content')

<div style="position: relative;">
    <div class="homepage-image" style="background-image: url('/images/bg_4.jpg');">
        <div class="container">
            <div class="row align-items-center justify-content-center text-center">
                <div class="col-md-7">
                    <h2 class="heading mb-5">Better To Give Than To Receive</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="site-section fund-raisers">
    <div class="container">
        <div class="row mb-3 justify-content-center">
            <div class="col-md-8 text-center">
                <h2>Our Donors</h2>
                <p class="lead mb-5">Your donations can save the lives of our frontliners. Let us help them so that they can save us!.</p>
            </div>
        </div>
        <div class="row">
           @foreach($donors as $donor)
                <div class="col-md-6 col-lg-3 mb-5">
                    <div class="person-donate text-center">
                        <img src="{{ (\App\User::getImage($donor->user_id)) }}" alt="Image placeholder" class="img-fluid">
                        <div class="donate-info">
                            <h2>{{ \App\User::getUser($donor->user_id)->name }}</h2>
                            <span class="time d-block mb-3">Donated {{ App\Common::get_time_difference_php($donor->created_at) }}</span>
                            <p>Donated <span class="text-success">₱{{ $donor->amount }}</span></p>
                        </div>
                    </div>
                </div>
           @endforeach
        </div>
    </div>
    <div class="col-12 text-center">
        {!! $donors->links('layouts.paginator') !!}
    </div>

</div>

@endsection


<style>
.site-section {
    padding: 4em 0;
}
.person-donate img {
    border-radius: 7px;
    width: 180px;
    margin-bottom: 20px;
}
.person-donate h2 {
    font-size: 20px;
    margin: 0;
    padding: 0;
}
.person-donate .time {
    color: #b3b3b3;
    font-size: 13px;
}
.person-donate p {
    line-height: 1.5;
}
.text-success {
    color: #28a745!important;
}
.person-donate em {
    font-family: serif;
}

.fundraise-item {
    -webkit-box-shadow: 0 0 40px -10px rgba(0, 0, 0, 0.1);
    box-shadow: 0 0 40px -10px rgba(0, 0, 0, 0.1);
    border: none;
    height: 100%;
}


.donors-pagination ul {
    padding: 0;
    margin: 0;
}
.donors-pagination ul li {
    display: inline-block;
    margin-bottom: 4px;
    font-weight: 400;
}
.donors-pagination ul li a, .donors-pagination ul li span {
    color: #152f4f;
    text-align: center;
    display: inline-block;
    width: 40px;
    height: 40px;
    line-height: 37px;
    border-radius: 50%;
    font-weight: 600;
    border: 1px solid #acb7c2;
}
.donors-pagination ul li.active a, .donors-pagination ul li.active span {
    background: #acb7c2;
    color: #152f4f;
    border: 1px solid transparent;
}
.donors-pagination a:hover {
    text-decoration: none;
}
</style>