@extends('layouts.app')

@section('content')
    <div class="container" style="padding-top:8em; padding-bottom:3em; ">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Your Donation will make a difference</div>

                    <div class="card-body">
                    <form method="POST" id="donation-form" class="form-horizontal" enctype="multipart/form-data" action="{{ route('donate.store') }}">
                        <div class="errors"></div>
                            {{csrf_field()}}
                        @if(!auth()->check())
                            <h5>Basic Information (Optional)</h5>
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Add Image') }}</label>
                                <div class="col-md-6">
                                    <div class="center">
                                        <div class="default">
                                            <img onclick="onImage()" src="{{asset('images/default.png')}}" height="100" width="100" id="im" name="im" class="rounded-circle">
                                            <input type="file" onchange="onFileSelected(event)" name="image" id="image" accept="image/*" value="submit" style="display: none;" >
                                        </div>
                                        <span class="icon-camera"> <i class="fa fa-camera"></i></span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('First Name') }}</label>
                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" placeholder="(Optional)" name="first_name" value="{{ old('name') }}" autocomplete="name" autofocus>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Last Name') }}</label>
                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" placeholder="(Optional)" name="last_name" value="{{ old('name') }}" autocomplete="name" autofocus>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Email') }}</label>
                                <div class="col-md-6">
                                    <input id="name" type="email" class="form-control" placeholder="(Optional)" name="email"  value="{{ old('name') }}" autocomplete="name" autofocus>
                                </div>
                            </div>
                        @endif
                        <h5>Donation Details</h5>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Amount') }}</label>
                            <div class="col-md-6">
                                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                    <label class="btn btn-secondary active" >
                                        <input type="radio" name="options"  onclick="onAmount('100')" id="option1" autocomplete="off" checked> PHP100
                                    </label>
                                    <label class="btn btn-secondary">
                                        <input type="radio" name="options" onclick="onAmount('200')" id="option2" autocomplete="off"> PHP200
                                    </label>
                                    <label class="btn btn-secondary ">
                                        <input type="radio" name="options" onclick="onAmount('500')" id="option3" autocomplete="off"> PHP500
                                    </label>
                                    <label class="btn btn-secondary">
                                        <input type="radio" name="options" onclick="onAmount('1000')" id="option4" autocomplete="off"> PHP1000
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Enter Specific Amount') }}</label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">₱</span>
                                    </div>
                                    <input type="text" class="form-control amount" aria-label="Amount (to the nearest dollar)" name="amount" required>

                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="attachment" class="col-md-4 col-form-label text-md-right">{{ __('Attached Proof Donation') }}</label>
                            <div class="col-md-6">
                                <input id="attachment" type="file" name="attachment" accept="image/*">
                            </div>
                            <input type="hidden" name="id" value="">
                        </div>
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Comment') }}</label>
                                <div class="col-md-6">
                                    <textarea class="form-control" name="comment" placeholder="(Optional)"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6 offset-md-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="hide_info" value="1">

                                        <label class="form-check-label" for="remember">
                                            {{ __('Hide your name and comment from everyone') }}
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6 offset-md-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="hide_amount" value="1">

                                        <label class="form-check-label" for="remember">
                                            {{ __('Hide your donation amount') }}
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <h5>Donate thru:</h5>
                            <div class="row">
                                <div class="offset-md-2 col-md-3 text-center">
                                    <a href="#" class="popup_form"  data-url="{{ route('gcash') }}" data-toggle="modal" title="Gcash Information" >
                                        <img src="{{url('/images/gcash_logo.png')}}" class="" style="height: auto;width: 100px;padding-top: 25px;">
                                    </a>
                                    <a href="#" class="popup_form a-info-icon" data-url="{{ route('gcash') }}">View GCash</a>
                                </div>
                                
                                <div class="col-md-3 text-center">
                                    <a href="#" class="popup_form" data-url="{{ route('bank') }}" data-toggle="modal" title="Bank Information" >
                                        <img src="{{url('/images/bank_logo1.png')}}" class="" style="height: auto;width: 100px;">
                                    </a>
                                    <a href="#" class="popup_form a-info-icon" data-url="{{ route('bank') }}" data-toggle="modal">View Bank</a>
                                </div>
                                <div class="col-md-3 text-center">
                                    <a href="https://www.paypal.com/paypalme2/abcsystems" target="_blank">
                                        <img src="{{url('/images/paypal.png')}}" class="" style="height: auto;width: 100px;padding-top: 20px;">
                                    </a>
                                    <a class="a-info-icon" href="https://www.paypal.com/paypalme2/abcsystems" target="_blank">View PayPal</a>
                                </div>
                            </div>
                            <!-- <div class="form-radio-flex" style="margin-bottom: 10px">   
                                <input type="radio" name="payment_type" id="payment_visa" value="payment_visa" checked="checked" class="valid" aria-invalid="false">    
                                <label for="payment_visa"><img src="images/gcash.png" alt=""></label>   
                                <input type="radio" name="payment_type" id="payment_master" value="payment_master" class="valid" aria-invalid="false">  
                                <label for="payment_master"><img src="images/icon-master.png" alt=""></label>   
                                <input type="radio" name="payment_type" id="payment_paypal" value="payment_paypal" class="valid" aria-invalid="false">  
                                <label for="payment_paypal"><img src="images/icon-paypal.png" alt=""></label>   
                            </div>   -->
                            {{--<hr>--}}
                            {{--<h5>Proof of Donation</h5>--}}
                            {{--<input id="file-upload1" type="file"/>--}}
                            {{--<a href="#" class="btn btn-secondary">Skip for Now</a>--}}
                            {{--<div class="alert alert-info" role="alert">--}}
                                {{--Please upload your proof of donation here--}}
                            {{--</div> --}}
                            <div align="right" style="margin-top: 20px;">
                                <button type="submit" class="button-medium btn btn-primary">Submit Donation</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Mark wizard form --}}
    {{--<div class="block-31" style="position: relative;">--}}
        {{--<div class="owl-carousel loop-block-31 ">--}}
          {{--<div class="block-30 block-30-sm item" style="background-image: url('/images/bg_1.jpg');" data-stellar-background-ratio="0.5">--}}
              {{--<div class="container">--}}
                {{--<div class="row align-items-center justify-content-center text-center">--}}
                    {{--<div class="col-md-7">--}}
                      {{--<h2 class="heading">Donate Now</h2>--}}
                      {{--<p class="xs-inner-banner-content">Give a helping hand for poor people</p>--}}
                      {{--<a class="button-medium" href="#"> <i class="fa fa-fire"></i>&nbsp;Donate Now</a>--}}
                    {{--</div>  --}}
                {{--</div>--}}
              {{--</div>--}}
          {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<section class="xs-section-padding bg-gray">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-6">--}}
                    {{--<div class="xs-donation-form-images">--}}
                        {{--<img src="/images/body-bg.jpg" class="img-responsive" style="max-width: 100%; height: 765px" alt="Family Images">--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-6">--}}
                {{--<div class="card px-0 pt-4 pb-0 mt-3 mb-3">--}}
                    {{--<h2 class="text-center"><strong style="font-size: 1.2em">Make a donation</strong></h2>--}}
                    {{--<p class="text-center">To learn more about make donate charity with us visit our Website</p>--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-md-12 mx-0">--}}
                            {{--<form id="msform">--}}
                                {{--<!-- progressbar -->--}}
                                {{--<ul id="progressbar">--}}
                                    {{--<li class="active" id="account"><strong>Personal</strong></li>--}}
                                    {{--<li id="personal"><strong>Payment</strong></li>--}}
                                    {{--<li id="payment"><strong>Proof of Payment</strong></li>--}}
                                    {{--<li id="confirm"><strong>Finish</strong></li>--}}
                                {{--</ul> <!-- fieldsets -->--}}
                                {{--<fieldset>--}}
                                    {{--<div class="form-card">--}}
                                        {{--<h2 class="fs-title">Personal Information</h2> --}}
                                        {{--<h5>Image</h5>--}}
                                        {{--<input id="file-upload" type="file"/>--}}
                                        {{--<input type="email" name="email" placeholder="Email Address"> --}}
                                        {{--<input type="text" name="first name" placeholder="First Name"> --}}
                                        {{--<input type="text" name="last name" placeholder="Last Name"> --}}
                                        {{--<div class="form-check">--}}
                                            {{--<input class="form-check-input" type="checkbox" name="hide_info" value="1">--}}
                                            {{--<label class="form-check-label" for="remember" style="margin-left: 10px">--}}
                                                {{--{{ __('Hide your name and comment from everyone') }}--}}
                                            {{--</label>--}}
                                        {{--</div>--}}
                                        {{--<div class="form-check">--}}
                                            {{--<input class="form-check-input" type="checkbox" name="hide_amount" value="1">--}}
                                            {{--<label class="form-check-label" for="remember" style="margin-left: 10px">--}}
                                                {{--{{ __('Hide your donation amount') }}--}}
                                            {{--</label>--}}
                                        {{--</div>--}}
                                    {{--</div> --}}
                                    {{--<input type="button" name="next" class="next action-button" value="NEXT">--}}
                                {{--</fieldset>--}}
                                {{--<fieldset>--}}
                                    {{--<div class="form-card">--}}
                                        {{--<h2 class="fs-title">Payment Information</h2> --}}
                                        {{--<h5>Payment Type</h5>--}}
                                        {{--<div class="form-radio-flex" style="margin-bottom: 10px">--}}
                                            {{--<input type="radio" name="payment_type" id="payment_visa" value="payment_visa" checked="checked" class="valid" aria-invalid="false">--}}
                                            {{--<label for="payment_visa"><img src="images/gcash.png" alt=""></label>--}}
                                            {{--<input type="radio" name="payment_type" id="payment_master" value="payment_master" class="valid" aria-invalid="false">--}}
                                            {{--<label for="payment_master"><img src="images/icon-master.png" alt=""></label>--}}
                                            {{--<input type="radio" name="payment_type" id="payment_paypal" value="payment_paypal" class="valid" aria-invalid="false">--}}
                                            {{--<label for="payment_paypal"><img src="images/icon-paypal.png" alt=""></label>--}}
                                        {{--</div>--}}
                                        {{--<div class="btn-group btn-group-toggle" data-toggle="buttons" style="margin-bottom: 10px">--}}
                                            {{--<label class="btn btn-secondary active" >--}}
                                                {{--<input type="radio" name="options"  onclick="onAmount('100')" id="option1" autocomplete="off" checked> PHP100--}}
                                            {{--</label>--}}
                                            {{--<label class="btn btn-secondary">--}}
                                                {{--<input type="radio" name="options" onclick="onAmount('200')" id="option2" autocomplete="off"> PHP200--}}
                                            {{--</label>--}}
                                            {{--<label class="btn btn-secondary ">--}}
                                                {{--<input type="radio" name="options" onclick="onAmount('500')" id="option3" autocomplete="off"> PHP500--}}
                                            {{--</label>--}}
                                            {{--<label class="btn btn-secondary">--}}
                                                {{--<input type="radio" name="options" onclick="onAmount('1000')" id="option4" autocomplete="off"> PHP1000--}}
                                            {{--</label>--}}
                                        {{--</div>--}}
                                        {{--<input type="text" name="Amount" placeholder="₱"> --}}
                                        {{--<input type="text" name="Account Name" value="Aries P. Alibangbang"> --}}
                                        {{--<div class="col-12" style="display: inline-flex;padding-right: 0px">--}}
                                            {{--<input class="form-check-input" type="checkbox">--}}
                                            {{--<input type="text" name="BPI Account" value="BPI: 4030-0127-18">--}}
                                        {{--</div>--}}
                                        {{--<div class="col-12" style="display: inline-flex;padding-right: 0px">--}}
                                            {{--<input class="form-check-input" type="checkbox">--}}
                                            {{--<input type="text" name="Metrobank Account" value="Metrobank: 031-3-03168996-2">--}}
                                        {{--</div>--}}
                                    {{--</div> --}}
                                    {{--<input type="button" name="previous" class="previous action-button-previous" value="Previous"> --}}
                                    {{--<input type="button" name="next" class="next action-button" value="NEXT">--}}
                                {{--</fieldset>--}}
                                {{--<fieldset>--}}
                                    {{--<div class="form-card">--}}
                                        {{--<h2 class="fs-title">Proof of Payment</h2>--}}
                                        {{--<input id="file-upload1" type="file"/>--}}
                                        {{--<div class="alert alert-info" role="alert">Please upload your proof of donation here</div>--}}
                                    {{--</div>    --}}
                                    {{--<input type="button" style="position: relative" name="previous" class="previous action-button-previous" value="Previous"> --}}
                                    {{--<input type="button" name="skip" class="skip action-button-skip" value="Skip"> --}}
                                    {{--<input type="button" name="make_payment" class="next action-button" value="Submit">--}}
                                {{--</fieldset>--}}
                                {{--<fieldset>--}}
                                    {{--<div class="form-card">--}}
                                        {{--<h2 class="fs-title text-center">Success !</h2> <br><br>--}}
                                        {{--<div class="row justify-content-center">--}}
                                            {{--<div class="col-3"> <img src="https://img.icons8.com/color/96/000000/ok--v2.png" class="fit-image"> </div>--}}
                                        {{--</div> <br><br>--}}
                                        {{--<div class="row justify-content-center">--}}
                                            {{--<div class="col-7 text-center">--}}
                                                {{--<h5>You Have Successfully Signed Up</h5>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</fieldset>--}}
                            {{--</form>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</section>--}}
@push('scripts')
    <!-- {{--<script>--}}
        {{--$(document).ready(function(){--}}

        {{--var current_fs, next_fs, previous_fs; //fieldsets--}}
        {{--var opacity;--}}

        {{--$(".next").click(function(){--}}

        {{--current_fs = $(this).parent();--}}
        {{--next_fs = $(this).parent().next();--}}

        {{--//Add Class Active--}}
        {{--$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");--}}

        {{--//show the next fieldset--}}
        {{--next_fs.show();--}}
        {{--//hide the current fieldset with style--}}
        {{--current_fs.animate({opacity: 0}, {--}}
        {{--step: function(now) {--}}
        {{--// for making fielset appear animation--}}
        {{--opacity = 1 - now;--}}

        {{--current_fs.css({--}}
        {{--'display': 'none',--}}
        {{--'position': 'relative'--}}
        {{--});--}}
        {{--next_fs.css({'opacity': opacity});--}}
        {{--},--}}
        {{--duration: 600--}}
        {{--});--}}
        {{--});--}}

        {{--$(".previous").click(function(){--}}

        {{--current_fs = $(this).parent();--}}
        {{--previous_fs = $(this).parent().prev();--}}

        {{--//Remove class active--}}
        {{--$("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");--}}

        {{--//show the previous fieldset--}}
        {{--previous_fs.show();--}}

        {{--//hide the current fieldset with style--}}
        {{--current_fs.animate({opacity: 0}, {--}}
        {{--step: function(now) {--}}
        {{--// for making fielset appear animation--}}
        {{--opacity = 1 - now;--}}

        {{--current_fs.css({--}}
        {{--'display': 'none',--}}
        {{--'position': 'relative'--}}
        {{--});--}}
        {{--previous_fs.css({'opacity': opacity});--}}
        {{--},--}}
        {{--duration: 600--}}
        {{--});--}}
        {{--});--}}

        {{--$('.radio-group .radio').click(function(){--}}
        {{--$(this).parent().find('.radio').removeClass('selected');--}}
        {{--$(this).addClass('selected');--}}
        {{--});--}}

        {{--$(".submit").click(function(){--}}
        {{--return false;--}}
        {{--})--}}

        {{--});--}}
    {{--</script>--}} -->

  <script>
        function onAmount($amnt)
        {
            $('.amount').val($amnt);
        }
        let rules = {
            amount : {
                required: true
            },
            attachment : {
                required: true
            },
        };
        $('#donation-form').registerFields(rules);

        //function for uploading image
        function onImage() {
            $('#image').click();
        }
        //function for uploading image
        function onFileSelected() {
            var selectedFile = event.target.files[0];
            var files = event.target.files;
            var reader = new FileReader();
            if(selectedFile && selectedFile.size < 2097152)
            {
                var imgtag = document.getElementById("im");
                imgtag.title = selectedFile.name;
                reader.onload =  function(event) {
                    imageIsLoaded(event);
                    imgtag.src = event.target.result;
                    
                };
                reader.readAsDataURL(selectedFile);
                
            }
        }
        //function for uploading image
        function imageIsLoaded(e) {
            picture = e.target.result;
        }

    </script>
@endpush
@endsection

<style>
    .xs-inner-banner-content {
        font-size: 1.57143em;
        margin-bottom: 15px;
        line-height: 1.5;
        color:#FFF;
    }
    .form-check-input {
        width: 5% !important;
        margin-top: 5px !important;
    }

    .xs-donation-form-images {
        -webkit-box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
        box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
        margin-top: 16px;
    }
    .form-radio-flex input {
        width: 0;
        height: 0;
        position: absolute;
        left: -9999px;
    }
    .form-radio-flex input:checked+label {
        border: 1px solid #1abc9c;
        z-index: 1;
    }
    .form-radio-flex input+label {
        margin: 0;
        padding: 12px 10px;
        width: 86px;
        height: 50px;
        box-sizing: border-box;
        position: relative;
        display: inline-block;
        text-align: center;
        background-color: transparent;
        border: 1px solid #ebebeb;
        text-align: center;
        text-transform: none;
        transition: border-color .15s ease-out,color .25s ease-out,background-color .15s ease-out,box-shadow .15s ease-out;
        border-radius: 5px;
        -moz-border-radius: 5px;
        -webkit-border-radius: 5px;
        -o-border-radius: 5px;
        -ms-border-radius: 5px;
    }
    .a-info-icon {
        display: flow-root;
        margin-top: 10px;
        /* color: #0f0f0f; */
    }
    .a-info-icon:hover {
        text-decoration: none;
        color: #0f0f0f;
    }
</style>

<!-- <style>


#grad1 {
    background-color: : #9C27B0;
    background-image: linear-gradient(120deg, #FF4081, #81D4FA)
}

#msform {
    text-align: center;
    position: relative;
    margin-top: 20px
}

#msform fieldset .form-card {
    background: white;
    border: 0 none;
    border-radius: 0px;
    box-shadow: 0 2px 2px 2px rgba(0, 0, 0, 0.2);
    padding: 20px 40px 30px 40px;
    box-sizing: border-box;
    width: 94%;
    margin: 0 3% 20px 3%;
    position: relative
}

#msform fieldset {
    background: white;
    border: 0 none;
    border-radius: 0.5rem;
    box-sizing: border-box;
    width: 100%;
    margin: 0;
    padding-bottom: 20px;
    position: relative
}

#msform fieldset:not(:first-of-type) {
    display: none
}

#msform fieldset .form-card {
    text-align: left;
    color: #9E9E9E
}

#msform input,
#msform textarea {
    padding: 0px 8px 4px 8px;
    border: none;
    border-bottom: 1px solid #ccc;
    border-radius: 0px;
    margin-bottom: 25px;
    margin-top: 2px;
    width: 100%;
    box-sizing: border-box;
    font-family: montserrat;
    color: #2C3E50;
    font-size: 16px;
    letter-spacing: 1px
}

#msform input:focus,
#msform textarea:focus {
    -moz-box-shadow: none !important;
    -webkit-box-shadow: none !important;
    box-shadow: none !important;
    border: none;
    font-weight: bold;
    border-bottom: 2px solid skyblue;
    outline-width: 0
}

#msform .action-button {
    width: 120px;
    background: #70e040;
    color: white;
    float: right;
    margin-right: 15px;
    border-radius: 5px;
    cursor: pointer;
    font-family: 'Lato', sans-serif, arial;
    padding: 10px 5px;
}
.action-button-skip {
    width: 120px !important;
    background: #227dc7 !important;
    font-family: 'Lato', sans-serif, arial !important;
    color: white !important;
    position: absolute !important;
    border-radius: 5px !important;
    cursor: pointer !important;
    padding: 10px 5px !important;
    margin-left: 10px !important;
}

#msform .action-button-previous {
    width: 120px;
    background: #616161;
    font-family: 'Lato', sans-serif, arial;
    color: white;
    position: absolute;
    border-radius: 5px;
    cursor: pointer;
    padding: 10px 5px;
    margin-left: 5px;
}


select.list-dt {
    border: none;
    outline: 0;
    border-bottom: 1px solid #ccc;
    padding: 2px 5px 3px 5px;
    margin: 2px
}

select.list-dt:focus {
    border-bottom: 2px solid skyblue
}

.card {
    z-index: 0;
    border: none;
    border-radius: 0.5rem;
    position: relative
}

.fs-title {
    font-size: 25px;
    color: #2C3E50;
    margin-bottom: 15px;
    font-weight: bold;
    text-align: left
}

#progressbar {
    margin-bottom: 30px;
    overflow: hidden;
    color: lightgrey
}

#progressbar .active {
    color: #000000
}

#progressbar li {
    list-style-type: none;
    font-size: 12px;
    width: 25%;
    float: left;
    position: relative
}

#progressbar #account:before {
    font-family: FontAwesome;
    content: "\f023"
}

#progressbar #personal:before {
    font-family: FontAwesome;
    content: "\f007"
}

#progressbar #payment:before {
    font-family: FontAwesome;
    content: "\f09d"
}

#progressbar #confirm:before {
    font-family: FontAwesome;
    content: "\f00c"
}

#progressbar li:before {
    width: 50px;
    height: 50px;
    line-height: 45px;
    display: block;
    font-size: 18px;
    color: #ffffff;
    background: lightgray;
    border-radius: 50%;
    margin: 0 auto 10px auto;
    padding: 2px
}

#progressbar li:after {
    content: '';
    width: 100%;
    height: 2px;
    background: lightgray;
    position: absolute;
    left: 0;
    top: 25px;
    z-index: -1
}

#progressbar li.active:before,
#progressbar li.active:after {
    background: skyblue
}

.radio-group {
    position: relative;
    margin-bottom: 25px
}

.radio {
    display: inline-block;
    width: 204;
    height: 104;
    border-radius: 0;
    background: lightblue;
    box-shadow: 0 2px 2px 2px rgba(0, 0, 0, 0.2);
    box-sizing: border-box;
    cursor: pointer;
    margin: 8px 2px
}

.radio:hover {
    box-shadow: 2px 2px 2px 2px rgba(0, 0, 0, 0.3)
}

.radio.selected {
    box-shadow: 1px 1px 2px 2px rgba(0, 0, 0, 0.1)
}

.fit-image {
    width: 100%;
    object-fit: cover
}
</style> -->
