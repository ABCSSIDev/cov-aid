@extends('layouts.app')

@section('content')
<div class="container" style="padding-top: 7em">
    <div class="text-center">
        <img src="/images/thank-you.png" style="max-width: 100%; height: 130px">
    </div>
    <div class="col-md-11 offset-md-1">
        <p class="lead"><strong style="font-weight: 600;">Dear Mr./Ms. {{ $donation->userData->name }},</strong>
        <br>
        <br>
        Thank you for your generosity! We, at ABCSSI and DBS greatly appreciate your donation. Your support helps to further our mission through this fundraising campaign.
        <br>
        <br>
        Your support is invaluable to us, maraming salamat po! If you have specific questions about our mission be sure to visit our website (<a href="https://abcsystems.com.ph/">abcsystems.com.ph</a>) or facebook pages (<a href="https://www.facebook.com/ABCsyssol/">ABCSSI</a>) or call (63)2 241-8332!
        <br>
        <br>
        Sincerely,
        COVAiD Team PH.
    </div>
    <hr>
    <div class="text-center">
        <!-- <p>
            Having trouble? <a href="">Contact us</a>
        </p> -->
        <p class="lead">
            <a class="btn btn-primary button-medium" href="{{route('home')}}" role="button">Continue to homepage</a>
        </p>
    </div>
</div>


    <!-- <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                Dear (Name),

                Thank you for your generosity! We, at ABCSSI and DBS greatly appreciate your donation. Your support helps to further our mission through this fundraising campaign.

                Your support is invaluable to us, maraming salamat po! If you have specific questions about our mission be sure to visit our website (link) or facebook pages ( ) or call [contact details]!

                Sincerely,
                COVAiD Team PH
            </div>
        </div>
    </div> -->
@endsection