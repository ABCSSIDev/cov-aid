
            <table width="100%" cellpadding="0" cellspacing="0" style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;margin:0;padding:0;width:100%">
                    <tbody>
                        <tr>
                            <td style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;padding:25px 0;text-align:center;background-color:#152F4F;width:70%;">
                                <a href="http://localhost" style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;color:#ffffff;font-size:19px;font-weight:bold;text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://localhost&amp;source=gmail&amp;ust=1585830085088000&amp;usg=AFQjCNEbc0nGE53rE5BtPM5e05oKjF_F1Q">
                                <span class="il">COV-AID</span>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td width="100%" cellpadding="0" cellspacing="0" style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;background-color:#ffffff;border-bottom:1px solid #edeff2;border-top:1px solid #edeff2;margin:0;padding:0;width:100%">
                                <h2 style="margin-left: 25%;margin-top:5%;font-size: 2rem;">We are grateful for your donation</h2>
                                <!-- <p style="margin-left: 33%">Click the button below to verify your donation</p>
                                @component('mail::button', ['url' => $url])
                                Click to Verifiy
                                @endcomponent -->
                                <br>
                                <p>Thank you for your generosity! We, at ABCSSI and DBS greatly appreciate your donation. Your support helps to further our mission through this fundraising campaign.</p>
                                <br>
                                <br>
                                <p>Your support is invaluable to us, maraming salamat po! If you have specific questions about our mission be sure to visit our website (<a href="https://abcsystems.com.ph/">abcsystems.com.ph</a>) or facebook pages (<a href="https://www.facebook.com/ABCsyssol/">ABCSSI</a>) or call (63)2 241-8332!</p>
                                <br>
                                <br>
                                Sincerely,
                                COVAiD Team PH.
                            </td>
                        </tr>
                    </tbody>
            </table>



