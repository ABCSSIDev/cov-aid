@extends('layouts.app')

@section('content')
<div class="container" style="padding-top: 8em; padding-bottom: 3em">
    <div class="row justify-content-center">
        <div class="col-md-12">
        <div align="right" style=""><a href="{{ route('donation.index') }}"><button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a></div>
            <div class="card">
                <div class="card-header">
                    Donation Details 
                    @if($donor_details->verified_by != null)
                    <span class="badge badge-success">VERIFIED <i class="fa fa-check" aria-hidden="true"></i></span>
                    @else
                    <span class="badge badge-secondary">For Verification </span>
                    @endIf
                </div>

                <div class="card-body">
                    <form method="POST" action="">
                        @csrf
                        <h3>Donor Information</h3>
                            @if($donor_details->verified_by == null)
                            <div class="form-group" align="right" >
                                <a href="#" class="btn btn-primary popup_form button-medium" data-url="{{ route('verify.show', $donor_details->id) }}" data-toggle="modal" title="{{ $donor_details->donorData->userData->name }} - ₱{{ number_format($donor_details->donorData->amount,2) }}" >VERIFY NOW</a>
                            </div>
                            @endIf
                        <hr>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Name:</label>
                                <p><b>{{ $donor_details->donorData->userData->name }}</b></p>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Email:</label>
                                <p><b>{{ $donor_details->donorData->userData->email }}</b></p>
                            </div>
                        </div>
                        <h3>Donation Details</h3><hr>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Amount Donated:</label>
                                <p><b>{{ $donor_details->donorData->amount }}</b></p>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Hide amount?</label>
                                <p><b>{{ ($donor_details->donorData->hide_amount == 0 ? 'No' : 'Yes') }}</b></p>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Hide Information?</label>
                                <p><b>{{ ($donor_details->donorData->hide_info == 0 ? 'No' : 'Yes') }}</b></p>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Hide Comment?</label>
                                <p><b>{{ ($donor_details->donorData->hide_comment == 0 ? 'No' : 'Yes') }}</b></p>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Donation Date:</label>
                                <p><b>{{ date('F d, Y H:i A',strtotime($donor_details->donorData->created_at)) }}</b></p>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Proof of Donation:</label>
                                <img style="width: 70%;" src="{{ route('donation.image',$donor_details->id) }}" alt="">
                            </div>
                        </div>
                        <hr>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Comment:</label>
                                <p><b>{{ (is_null($donor_details->donorData->comment) ? '-None-' : $donor_details->donorData->comment) }}</b></p>
                            </div>
                        </div>
                        @if($donor_details->verified_by != null)
                        <h3>Verification Details</h3>
                        <hr>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Verified By:</label>
                                <p>{{ $donor_details->verifiedBy->name }}</p>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Verified Date:</label>
                                <p>{{ date('F d, Y', strtotime($donor_details->verified_date)) }}</p>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Donation Sent:</label>
                                <p>
                                    @if($donor_details->donation_method == 1)
                                        GCash
                                    @elseif($donor_details->donation_method == 2)
                                        BPI
                                    @elseif($donor_details->donation_method == 3)
                                        MetroBank
                                    @else
                                        PayPal
                                    @endIf
                                </p>
                            </div>
                        </div>
                        @endIf
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
