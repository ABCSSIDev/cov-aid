@extends('layouts.app')

@section('content')
    <div class="container" style="padding-top:8em; padding-bottom:3em; ">
        <div class="container" style="padding-top:8em; padding-bottom:3em; ">

            <h4 class="form-head"></h4>
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div align="right" style="margin: 0px 0px 20px 0px">
                        <a href="{{ route('donation.index') }}">
                            <button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a>
                    </div>
                </div>
            </div>
            <form method="POST" action="{{ route('donation.store') }}" id="donation_form" class="form-horizontal" enctype="multipart/form-data">
                @csrf
                <div class="row justify-content-center">
                    <div class="col-md-12">

                        <table class="table table-striped table-bordered table-hover text-center table-mediascreen" id="donation_create_list">
                            <thead class="thead-dark">
                            <tr align="center">
                                <th width="15%">Action</th>
                                <th width="25%">Donor</th>
                                <th width="20%">Donation Amount</th>
                                <th width="20%">Donate Thru</th>
                                <th width="20%">Proof of Donation</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    <button class="btn btn-primary" id="add_new"><span class="fa fa-plus"></span></button>
                                    <input type="hidden" name="for_insert[]" value="0">
                                </td>
                                <td>
                                    <input type="text" class="form-control" placeholder="Donor's Name (Optional)." name="donor0">
                                </td>
                                <td>
                                    <input type="text" class="form-control amount" placeholder="Amount in Peso" name="amount0">
                                </td>
                                <td>
                                    <select class="form-control" name="donation_send0">
                                        <option value="1">GCash</option>
                                        <option value="2">BPI</option>
                                        <option value="3">MetroBank</option>
                                        <option value="4">PayPal</option>
                                    </select>
                                </td>
                                <td>
                                    <input type="file" class="form-control" name="attachment0">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group" align="right" >
                            <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> SUBMIT</button>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>
@endsection
@push('scripts')
    <script>
        let rules = {
            amount0 : {
                required: true,
                min: 1
            },
        };
        $('#donation_form').registerFields(rules);
        let ctr = 1;
        $('#add_new').on('click',function (e) {
            e.preventDefault();
            let row = '<tr class="remove_row">' +
                '<td>' +
                '<button class="btn btn-danger remove" ><span class="fa fa-minus"></span></button>' +
                '<input type="hidden" name="for_insert[]" value="'+ctr+'">' +
                '</td>' +
                '<td>' +
                ' <input type="text" class="form-control" placeholder="Donor\'s Name (Optional)." name="donor'+ctr+'">' +
                '</td>' +
                '<td>' +
                '<input type="text" class="form-control amount" placeholder="Amount in Peso" name="amount'+ctr+'">' +
                '</td>' +
                '<td>' +
                    '<select class="form-control" name="donation_send'+ctr+'">'+
                        '<option value="1">GCash</option>'+
                        '<option value="2">BPI</option>'+
                        '<option value="3">MetroBank</option>'+
                        '<option value="4">PayPal</option>'+
                    '</select>'+
                '</td>' +
                '<td>' +
                '<input type="file" class="form-control" name="attachment'+ctr+'">' +
                '</td>' +
                '</tr>';
            $('#donation_create_list tbody').append(row);
            $('input[name="amount'+ctr+'"]').rules('add',{
                'required' : true,
                'min' : 1
            })
            ctr++;
        });
        $(document).on('click','.remove',function (e) {
            e.preventDefault();
            $(this).closest('tr').remove();
        });
    </script>
@endpush