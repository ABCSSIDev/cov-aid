@extends('layouts.app')

@section('content')
    <div class="container" style="padding-top:8em; padding-bottom:3em; ">

        <h4 class="form-head">Donors List</h4>
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{ route('donation.create') }}"><button class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> CREATE</button></a></div>
                <table class="table table-striped table-bordered table-hover text-center table-mediascreen" id="donation_list">
                    <thead class="thead-dark">
                    <tr align="center">
                        <th width="15%">Donation No.</th>
                        <th width="25%">Donor</th>
                        <th width="15%">Amount</th>
                        <th width="10%">Date Donated</th>
                        <th width="20%">Status</th>
                        <th width="15%">Action</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $("#donation_list").DataTable({
            pagingType: "full_numbers",
            processing: true,
            serverSide: true,
            type: "GET",
            ajax:"{!! route('get.donations.list') !!}",
            columns:[
                {data: "donation_id","name":"donation_id","width":"15%",
                    className:"v-align text-center",
                },
                {data: "donor","name":"donor","width":"20%", className:"v-align text-center"},
                {data: "amount","name":"amount","width":"20%", className:"v-align text-center"},
                {data: "date_donated","name":"date_donated","width":"30%", className:"v-align text-center"},
                {data: "status","name":"status","width":"20%", className:"v-align text-center"},
                {data: "action","name":"action",orderable:false,searchable:false,"width":"15%"},
            ],
            select: {
                style: "multi"
            }
        });
    </script>
@endpush