<form method="POST" action="{{ route('donation.update', $id) }}" id="verify-form" class="form-horizontal" enctype="multipart/form-data">
    @csrf
    <div class="errors"></div>
    <input type="hidden" name="_method" value="PUT" >
    <div class="form-group" style="margin-top: 20px">
        <div class="col-12" align="right">
            <div class="col-12">
                <div class="form-group row">
                    <label for="email" class="col-md-4 col-form-label text-md-right">Date Verified:</label>
                    <div class="col-md-6">
                        <input id="date_verified" type="text" class="form-control datepicker" name="date_verified">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="email" class="col-md-4 col-form-label text-md-right">Verified Amount:</label>
                    <div class="col-md-6">
                        <input id="date_verified" type="text" class="form-control amount" name="verified_amount" value="{{ $amount }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="email" class="col-md-4 col-form-label text-md-right">Donation Thru:</label>
                    <div class="col-md-6">
                        <select class="form-control" name="donation_send">
                            <option value="1">GCash</option>
                            <option value="2">BPI</option>
                            <option value="3">MetroBank</option>
                            <option value="4">PayPal</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="email" class="col-md-4 col-form-label text-md-right">Verified By:</label>
                    <div class="col-md-6" align="left">
                        <p>{{ Auth::User()->name }}</p>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary button-medium">Confirm</button>
            </div>
        </div>
    </div>
</form>
<script>
$(function () {
    //function to load bootstrap datetimepicker
    $('.datepicker').datetimepicker({
        format: 'L'
    });
});

let rules = {
    date_verified : {
        required: true
    },
    verified_amount : {
        required: true
    },
    donation_send : {
        required: true
    }
};
$('#verify-form').registerFields(rules);
</script>