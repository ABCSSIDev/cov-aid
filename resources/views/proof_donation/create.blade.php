<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta property="og:url" content="{{ route('home') }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{ config('app.name','Covaid.ph') }}" />
    <meta property="og:description" content="To" />
    <title>CoV-AiD.ph</title>
    <link rel="shortcut icon" href="{{ asset('images/covaid_landscape.png') }}" type="image/png">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">


    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <div id="fb-root"></div>


</head>
<body>
  <div id="app">
    <main class="py-4">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">{{ __('Submit your Proof of Donation') }}</div>

                        <div class="card-body">
                            <form method="POST" id="proof_donation-form" action="{{ route('proof_donation.store') }}" enctype="multipart/form-data" >
                                {{csrf_field()}}


                                <div class="errors"></div>
                                <div class="form-group row">
                                    <label for="attachment" class="col-md-4 col-form-label text-md-right">{{ __('Attach File') }}</label>
                                    <div class="col-md-6">
                                        <input id="attachment" type="file" name="attachment">
                                    </div>
                                    <input type="hidden" name="id" value="{{$id}}">
                                </div>

                                
                                <div class="form-group row mb-0">
                                    <div class="col-md-8 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Submit') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    </div>
</body>
</html>

@push('scripts')
    <script>
    let rules = {
        attachment : {
            required: true
        },
    };
    $('#proof_donation-form').registerFields(rules);

    //function for uploading image
    function onImage() {
        $('#attachment').click();
    }
    //function for uploading image
    function onFileSelected() {
        var selectedFile = event.target.files[0];
        var files = event.target.files;
        var reader = new FileReader();
        if(selectedFile && selectedFile.size < 2097152)
        {
            var imgtag = document.getElementById("im");
            imgtag.title = selectedFile.name;
            reader.onload =  function(event) {
                imageIsLoaded(event);
                imgtag.src = event.target.result;

            };
            reader.readAsDataURL(selectedFile);

        }
    }
    //function for uploading image
    function imageIsLoaded(e) {
        picture = e.target.result;
    }
    </script>
@endpush
