@extends('layouts.app')

@section('content')
<div class="container" style="padding-top: 8em">

  <div class="row">
    <div class="col-md-8 blog-main">
      <h3 class="border-bottom fundraising-content">
       
        <div class="toolbar-fundraising ">
          <span class="author"><i class="fa fa-user"></i>	102 <a href="#">Donors</a></span>
          <span class="cat-link"><i class="fa fa-thumbs-up"></i> 20 <a href="#">Likes</a></span>
          <span class="cat-comments author"><i class="fa fa-share"></i><a href="#">30 </a> <a href="#">Shares </a></span>
        </div>
      </h3>

      <div class="blog-post">
        <h2 class="blog-post-title">{{ $update->title }}</h2>
        <p class="blog-post-meta">Date Published: {{ date('F d, Y', strtotime($update->date_posted)) }} <span class="badge badge-info" style="color:white">@php echo App\Common::get_time_difference_php($update->date_posted); @endphp</span></p>
        {!! $update->description !!}
        @foreach($update->updateDetails as $images)
        <br><br>
        <img src="{{ route('update.image',$images->id) }}" width="70%">
        @endforeach
      </div><!-- /.blog-post -->

      <div class="blog-post">
        
      </div><!-- /.blog-post -->


    </div><!-- /.blog-main -->

    <aside class="col-md-4 blog-sidebar">
      <div class="p-04 mb-3 rounded">
          <a href="{{ route('donate.index') }}" class="button-medium btn btn-primary btn-lg btn-block btn-huge">DONATE NOW</a>
          <!-- <a href="{{ route('donate.index') }}" class="button-medium btn-share btn btn-primary btn-lg btn-block btn-huge">SHARE</a>   -->
      </div>

      <div class="container">
        <p class="intro">
          <div class="embed-responsive embed-responsive-4by3">
            <iframe width="620" height="415" src="https://www.youtube.com/embed/N-S_qZhqMWI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </div>
          <p ><a href="https://www.youtube.com/watch?v=N-S_qZhqMWI&feature=youtu.be&fbclid=IwAR04l3Ia1EHrXEtSlbIPhKiOneZN4MAOaM6N9qv4wP14EddZVHL2f3agJo4" style="color:black !important" target="_blank">ABC System Solutions Incorporated ’ vision is to be one of the trusted IT Solutions Partner... </a></a>
        </p>
      </div>
      <div class="sidebar-widget post-widget container">
        <div class="sidebar-title"><h4>Donors</h4></div>
        <div class="widget-content">
          @if(count($donations) != 0)
            @foreach($donations as $donation)
            <article class="post">
              <div class="thumb">
                <img src="{{ ($donation->donorData->userData->image == null ? asset('images/default.png') : route('account.image',$donation->donorData->user_id)) }}" alt="" class="img-responsive img-circle">
              </div>
              <h3>{{ ($donation->donorData->hide_info == '1' ? 'Anonymous': $donation->donorData->userData->name) }}</h3>
              @if($donation->donorData->hide_amount == '0')
              <div class="date"><b class="amount-donation">₱{{ $donation->donorData->amount }}</b> <p class="m-meta-list-item"></p></div>
              @endIf
            </article>
            @endforeach
            <a href="{{ route('show.donors')}}" align="right">View All ></a>
            @else
            <div class="alert alert-dark" role="alert">
              No Donor as of now
            </div>
          @endif
        </div>
    </div>

      <div class="p-4">
        <h4 class="font-italic">Link with us</h4>
        <ol class="list-unstyled">
          <!-- <li><a href="#">Instagram</a></li> -->
          <li><a href="https://www.facebook.com/deconbizsol/">DECON BUSINESS SOLUTIONS</a></li>
          <li><a href="https://www.facebook.com/ABCsyssol/">ABC SYSTEM SOLUTIONS INC.</a></li>
        </ol>
      </div>
    </aside><!-- /.blog-sidebar -->

  </div>
  @if(count($other_updates) != 0)
  <h3 class="pb-4 mb-4 font-italic border-bottom">See Other Updates</h3>
    <div class="row mb-2">
      @foreach($other_updates as $others)
      <div class="col-md-6">
        <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
          <div class="col p-4 d-flex flex-column position-static">
            <!-- <strong class="d-inline-block mb-2 text-primary">World</strong> -->
            <h3 class="mb-0">{{ $others->title }}</h3>
            <div class="mb-1 text-muted">@php echo App\Common::get_time_difference_php($others->date_posted); @endphp</div>
            <div class="card-text mb-auto content_data">{!! $others->description !!}</div>
            <a href="{{ route('update.show',$others->id) }}" class="stretched-link">Continue reading</a>
          </div>
          <div class="col-auto d-none d-lg-block">
            <!-- <svg class="bd-placeholder-img" width="200" height="250" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Thumbnail"><title>Placeholder</title><rect width="100%" height="100%" fill="#55595c"></rect><text x="50%" y="50%" fill="#eceeef" dy=".3em">Thumbnail</text></svg> -->
            <img src="{{ route('update.image',App\Common::getFirtImage($others->id)) }}"  width="200" height="250">
          </div>
        </div>
      </div>
      @endforeach
    </div>
    @endIf

</div>
@endsection
<style>
.content_data{
  height: 70px;
  overflow: hidden;
  display: inline-block;
  text-overflow: ellipsis;
  margin: 0;
}
.fundraising-content {
  margin-bottom: 15px;
  padding-bottom: 15px;
}
.toolbar-fundraising span.author, .toolbar-fundraising span.cat-link {
  border-right: 1px solid #ddd;
}
.toolbar-fundraising span {
  color: #444;
  display: inline-block;
  font-size: 12px;
  line-height: 14px;
  margin-right: 10px;
  padding-right: 10px;
  text-transform: uppercase;
}
.toolbar-fundraising span i {
  padding-right: 6px;
}
.toolbar-fundraising span a {
  color: #888;
  text-decoration: none !important;
}
.p-04 {
  padding: 0px 14px;
}
.btn-share {
  background-color: transparent !important;
  border: 1px solid #70E040 !important;
  color: black !important;
  text-shadow: none !important;
}




.sidebar-title {
  position: relative;
  padding-bottom: 5px;
  margin-bottom: 20px;
}

.sidebar-title:after {
  position: absolute;
  left: 0;
  bottom: 0;
  height: 3px;
  width: 45px;
  background-color: #70e040;
  content: "";
}
.post-widget .post {
  position: relative;
  padding-left: 80px;
  margin-bottom: 20px;
  padding-top: 5px;
  padding-bottom: 10px;
  border-bottom: 1px solid #e3e3e3;
}
.post-widget .post .thumb {
  position: absolute;
  left: 0;
  top: 0;
  height: 60px;
  width: 60px;
}
.post-widget .post .thumb img {
  display: block;
  width: 100%;
}
.post-widget .post h3 {
  font-size: 18px;
  font-family: 'Poppins', sans-serif;
}
.post-widget .post .date {
  line-height: 1.2em;
  color: #888888;
  font-weight: 300;
}
.m-meta-list-item {
  display: inline;
}
.m-meta-list-item::before {
  margin-left: 5px;
  margin-right: 5px;
  color: #DDD;
}
.m-meta-list-item::before {
  content: "•";
}
.amount-donation {
  font-weight: 600;
  color: black;
}
</style>