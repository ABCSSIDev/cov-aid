@extends('layouts.app')

@section('content')

    <div class="col-12" style="margin-top: 10em;margin-bottom: 10em">
        <div class="text-center">
            <h3 class="tracker-title">PHILIPPINES COVID-19 TRACKER</h3>
            <p class="is-green">Last updated {{$last_updated}} Philippine time</p>
        </div>
        <div class="row">
            <div class="col-1"></div>
            <div class="col-5 cases">
                <div class="row text-center">
                    <div class="col-2"></div>
                    <div class="col-2 case-item is-red">
                        <h6 class="cases-title">Confirmed</h6>
                        <h2 class="cases-content" id="total">{{number_format($total_cases, 0, '.', ',')}}</h2>
                    </div>
                    <div class="col-2 case-item is-blue">
                        <h6 class="cases-title">Active</h6>
                        <h2 class="cases-content">{{number_format(($total_cases - ($recovered+$death_count)), 0, '.', ',')??'0'}}</h2>
                    </div>
                    <div class="col-2 case-item is-green">
                        <h6 class="cases-title">Recovered</h6>
                        <h2 class="cases-content">{{number_format($recovered, 0, '.', ',')}}</h2>
                    </div>
                    <div class="col-2 case-item is-black">
                        <h6 class="cases-title">Deceased</h6>
                        <h2 class="cases-content">{{number_format($death_count, 0, '.', ',')}}</h2>
                    </div>
                    <div class="col-2"></div>
                </div>
                <div class="row" >

                    <div class="col-12 text-center" id="scrollbar-7" style="overflow:scroll;height:80vh;width:100%;overflow:auto">
                        <table class="table table-striped" >
                            <thead>
                            <tr>
                                <th scope="col">City/Province</th>
                                <th scope="col">Confirmed Cases</th>
                            </tr>
                            </thead>
                            <tbody>
                            <div>
                                @foreach($residences as $residence)
                                <tr>
                                    <th scope="row">{{$residence['city_municipality']}}</th>
                                    <td>{{$residence['count']}}</td>
                                </tr>
                            </div>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-5" >
                <div class="row">
                    <div class="col-1"></div>
                    <div class="col-10 c-chart" >
                        <h3 class="tracker-title">SPREAD TRENDS</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-1"></div>
                    <div class="col-10 c-chart" >
                        <canvas id="confirmed_chart" class="bg-red"></canvas>
                    </div>
                </div>
                <div class="row">
                    <div class="col-1"></div>
                    <div class="col-10 c-chart" >
                        <canvas id="recovered_death"></canvas>
                    </div>
                </div>
                <div class="row">
                    <div class="col-1"></div>
                    <div class="col-10 c-chart" >
                        <canvas id="age" ></canvas>
                    </div>
                </div>
            </div>
            <div class="col-1"></div>
        </div>
    </div>


    @push('scripts')
        <script>

            var jsonData = $.ajax({
                url: '{{ route("covid.cases") }}',
                dataType: 'json'
            }).done(function(results){
                var total_cases = $('#confirmed_chart');
                var myLineChart = new Chart(total_cases,{
                    "type":"line",
                    "data":{
                        "labels":results.deaths.death_dates.slice(Math.max(results.deaths.death_dates.length - 20, 1)),
                        "datasets":[
                            {
                                "label":"Total Confirmed Cases",
                                "data":results.confirmed.confirmed_counts.slice(Math.max(results.confirmed.confirmed_counts.length - 20, 1)),
                                "fill":false,
                                "borderColor":"rgb(255, 7, 58)",
                                "lineTension":0.5,


                            }
                        ]
                    },
                    "options":{
                        title: {
                            display: true,
                            text: 'CONFIRMED CASES',
                            fontStyle: 'bold',
                            fontColor: 'rgba(48, 52, 56, 0.97)'
                        },
                        legend: {
                            display: false
                        },
                        scales: {
                            xAxes: [{
                                gridLines: {
                                    display:false
                                }
                            }],
                            yAxes: [{
                                position: 'right',
                                gridLines: {
                                    display:false
                                },
                                stepSize: 2
                            }]
                        }
                    }
                });

                var recovered_death = $('#recovered_death');
                var recovered_death_chart = new Chart(recovered_death,{
                    "type":"line",
                    "data":{
                        "labels":results.deaths.death_dates.slice(Math.max(results.deaths.death_dates.length - 20, 1)),
                        "datasets":[
                            {
                                "label":"Deaths",
                                "data":results.deaths.death_counts.slice(Math.max(results.deaths.death_counts.length - 20, 1)),
                                "fill":false,
                                "borderColor":"rgb(108, 117, 125)",
                                "lineTension":0.5
                            },
                            {
                                "label":"Recovered",
                                "data":results.recovered.recovered_counts.slice(Math.max(results.recovered.recovered_counts.length - 20, 1)),
                                "fill":false,
                                "borderColor":"rgb(40, 167, 69)",
                                "lineTension":0.5
                            }
                        ]
                    },
                    "options":{
                        title: {
                            display: true,
                            text: 'Recovered Cases Vs. Deceased Cases',
                            fontStyle: 'bold',
                            fontColor: 'rgba(48, 52, 56, 0.97)'
                        },
                        legend: {
                            display: true,
                            position: 'bottom',
                        },
                        scales: {
                            xAxes: [{
                                gridLines: {
                                    display:false
                                }
                            }],
                            yAxes: [{
                                position: 'right',
                                gridLines: {
                                    display:false
                                },
                                stepSize: 2
                            }]
                        }
                    }
                });
                //Age Chart
                var age_chart = $('#age');
                var myAgeChart = new Chart(age_chart, {
                    type: 'bar',
                    data: {
                        "labels":results.age_chart.age_group,
                        "datasets":[

                            {
                                "label":"Male",
                                "data":results.age_chart.male,
                                "fill":true,
                                "stack": "Stack 0",
                                "borderColor":"rgb(75, 192, 192)",
                                "backgroundColor": "rgba(75, 192, 192)"
                            },
                            {
                                "label": "Female",
                                "data": results.age_chart.female,
                                "fill": true,
                                "stack": "Stack 0",
                                "borderColor": "rgb(255, 99, 132)",
                                "backgroundColor": "rgba(255, 99, 132)"
                            },
                            {
                                "label":"Total",
                                "data":results.age_chart.total,
                                "borderColor":"rgb(75, 192, 192)",
                                "backgroundColor": "rgba(75, 192, 192)",

                            }
                        ]
                    },
                    "options":{
                        title: {
                            display: true,
                            text: 'Confirmed Cases by Age Group',
                            fontStyle: 'bold',
                            fontColor: 'rgba(48, 52, 56, 0.97)'
                        },
                        legend: {
                            display: true,
                            position: 'bottom',
                        },
                        tooltips:{
                            mode: "index",
                            intersect: true
                        },
                        scales: {
                            xAxes: [{
                                stacked: true,
                                gridLines: {
                                    display:false
                                }
                            }],
                            yAxes: [{
                                position: 'right',
                                gridLines: {
                                    display:false
                                },
                            }]
                        }
                    }
                });
            });
        </script>
    @endpush
    <style>
    {{--COVID19TRACKER CSS--}}

        canvas .bg-red{
            background-color: rgba(238, 147, 171, 0.32);
        }
        .tracker-title{
            font-family: 'Roboto', sans-serif;
            font-weight: bolder;
            font-size: xx-large;
        }

        /*Count Cases CSS*/
        .cases{
            font-family: 'Roboto', sans-serif;
            align-items: center;
        }

        .cases .case-item{
            border-radius: 10px;
            padding: 20px
        }

        .cases .cases-title{
            font-weight: normal;
            font-size: 1vw;
        }

        .cases .cases-content{
            font-weight: bolder;
            font-size: 2vw;

        }
        .cases .is-red{
            color: rgb(255, 7, 58);
        }

        .cases .is-blue{
            color: rgb(0, 123, 255);
        }

        .is-green{
            color: rgb(40, 167, 69);
        }

        .cases .is-black{
            color: rgba(77, 83, 88, 0.97);
        }
        .cases .is-grey{
            color: rgb(108, 117, 125);
        }

        /*Table Scrollbar*/
        #scrollbar-7::-webkit-scrollbar-track
        {
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
            background-color: #F5F5F5;
            border-radius: 10px;
        }

        #scrollbar-7::-webkit-scrollbar
        {
            width: 10px;
            background-color: #F5F5F5;
        }

        #scrollbar-7::-webkit-scrollbar-thumb
        {
            border-radius: 10px;
            background-image: -webkit-gradient(linear,
            left bottom,
            left top,
            color-stop(0.44, rgb(122,153,217)),
            color-stop(0.72, rgb(73,125,189)),
            color-stop(0.86, rgb(28,58,148)));
        }

        .c-chart{
            margin: 15px;
            font-family: 'Roboto', sans-serif;
        }

        .c-chart .chart-title{
            font-weight: normal;
            font-size: 1vw;
        }
    {{--COVID19TRACKER CSS--}}

    </style>

@endsection
