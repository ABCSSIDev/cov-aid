

<div class="text-center" id="spinner-grow-in" hidden>
  <div class="spinner-grow text-success" role="status">
    <span class="sr-only">Loading...</span>
  </div>
</div>


<script>
  
  
</script>

<style>
#spinner-grow-in > .spinner-grow {
  opacity: 1.0;
}
#spinner-grow-in {
  position: fixed;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  z-index: 5000;
  background: #1d1c1cad;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}
#spinner-grow-in .spinner-grow {
  height: 110px;
  width: 110px;
  position: absolute;
  top: 45%;
  left: 50%;
  z-index: 1;
  margin: -50px 0 0 -50px;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: center;
  -ms-flex-align: center;
  align-items: center;
  -webkit-box-pack: center;
  -ms-flex-pack: center;
  justify-content: center;
}
</style>