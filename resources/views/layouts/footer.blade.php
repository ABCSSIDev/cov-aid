<div class="pt-5 footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-xs-12 about-company">
                <h2>CoV-AiD</h2>
                <p class="pr-5 text-white-50" style="font-size: 17px">DECON Business Solutions in partnership with ABC Systems Solutions Inc. wishes to help our frontliners gear up for the battle against the unknown. We are one with our frontliners in providing our warriors the proper gear in their quest to battle the corona virus.</p>
                <p><a href="https://www.facebook.com/ABCsyssol/"><i class="fab fa-facebook-square"></i></a><a href="#">&nbsp;&nbsp;<i class="fab fa-twitter-square"></i></a></p>
            </div>
            <div class="col-lg-3 col-xs-12 links">
                <h4 class="mt-lg-0 mt-sm-3">Fundraising Projects</h4>
                <ul class="m-0 p-0" style="font-size: 17px">
                    <li>- <a href="{{ route('fundraising.show',1) }}">Personal Protective Equipment Suit (PPE'S)</a></li>
                    <li>- <a href="{{ route('fundraising.show',2) }}">Food Packs and Grocery Items</a></li>
                    <li>- <a href="{{ route('fundraising.show',3) }}">Logistics for Perishable Goods</a></li>
                </ul>
            </div>
            <div class="col-lg-4 col-xs-12 location">
                <h4 class="mt-lg-0 mt-sm-4">Location</h4>
                <p style="font-size: 17px">207 Malinao Street Brgy. Highway Hills Mandaluyong City, 1550</p>
                <p style="font-size: 17px" class="mb-0"><i class="fa fa-phone mr-3"></i>(63)2 241-8332</p>
                <p style="font-size: 17px"><i class="far fa-envelope mr-3"></i>covid-aid@abcsystems.com.ph</p>
            </div>
        </div>  
        <div class="col mt-5 copyright row">
            <div class="col-10" style="display: inline-flex">
                <p class="mr-3" style="margin-top: 15px">© 2020. All Rights Reserved</p>
                <!-- <a href="" style="color:#FFF;margin-top: 15px"><p style="margin-right:5px">Terms</p></a> -->
                <a href="" style="color:#FFF;margin-top: 15px"><p style="margin-right:5px">Privacy Policy</p></a>
                <!-- <a href="" style="color:#FFF;margin-top: 15px"><p>Legal</p></a> -->
            </div>
            <div class="col-2" style="margin-top; 10">
                <div class="fb-like" data-href="{{ route('home') }}" data-width="" data-layout="button_count" data-action="like" data-size="large" data-share="true"></div>
            </div>
        </div>
    </div>
</div>



<style>
    .footer {
    background: #152F4F;
    color: white;
}
.footer .about-company a {
    color: white;
    -webkit-transition: color .2s;
    transition: color .2s;
}
.footer .about-company i {
    font-size: 25px;
}
@media (min-width: 992px) {
    .mt-lg-0, .my-lg-0 {
        margin-top: 0!important;
    }
}
.footer .links ul {
    list-style-type: none;
}
.footer .links li a {
    color: white;
    -webkit-transition: color .2s;
    transition: color .2s;
}
.footer .location i {
    font-size: 18px;
}
.mt-5, .my-5 {
    margin-top: 3rem!important;
}
.footer .copyright  {
    border-top: 1px solid rgba(255, 255, 255, 0.1);
}
.text-white-50 {
    color: rgba(255, 255, 255, 0.7)!important;

}
.pt-5, .py-5 {
    padding-top: 3rem!important;
}

.h2, h2 {
    font-size: 2rem;
}
</style>