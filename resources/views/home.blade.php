@extends('layouts.app')

@section('content')
<div style="position: relative;">
    <div class="homepage-image" style="background-image: url('/images/bg_1.jpg');">
        <div class="container">
            <div class="row align-items-center justify-content-center text-center">
                <div class="col-md-7">
                    <h2 class="heading mb-5">CoV-AiD </h2>
                    <h3 style="color:white;">Is a non-profit fundraising campaign for our fellow Filipinos fighting for COVID-19. With the initiative of ABC Systems Solutions Inc. and DECON Business Solutions we are appealing to all for your help.</h3>
                    <a class="button-medium" href="{{ route('donate.index') }}"> <i class="fa fa-fire"></i> Be a Donor </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-12" style="background-color: #3d8bda;padding: 100px;">
    <div class="container" >
        <div class="row align-items-center justify-content-center text-center">
            <div class="col-md-7" style="color: white">
            <h5 ><strong>WE THANK YOU FOR YOUR KIND DONATIONS!</strong></h5>
            <h1 class="heading mb-5"><strong>{{$donation_sum}}</strong></h1>
            <h5 ><strong>HELP US SUPPORT THOSE IN NEED DURING THIS COVID-19 CRISIS</strong></h5>
            </div>
        </div>
    </div>
</div>
    <section class="categories">
        <div class="weekly-news-area pt-50">
            <div class="container">
                <div class="weekly-wrapper">
                    <!-- section Tittle -->
                    <div class="col-lg-12">
                        <div class="section-tittle mb-30">
                            <h3>Join us in equipping our frontliners! Our collective efforts can go a long way. Every peso you donate will be used to provide our frontliners the following:</h3>
                        </div>
                    </div>
                    <div class="row">
                    @foreach($projects as $project)
                    <div class="col-md-4">
                    <div class="card mb-4 shadow-sm">
                        <img src="{{ $project[0]['image_thumb'] }}"  width="100%" height="225">
                        <div class="card-body">
                        <p><b>{{ $project[0]['title'] }}</b></p>
                        <div class="card-text content_data">{!! $project[0]['content_html'] !!}</div>
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="btn-group">
                                <a class="button-medium" href="{{ route('fundraising.show',$project[0]['id']) }}">View </a>
                                <!-- <a href="{{ route('fundraising.show',$project[0]['id']) }}" class="btn btn-primary my-2">View</a> -->
                            </div>
                            <small class="text-muted">April 1, 2020</small>
                        </div>
                        </div>
                    </div>
                    </div>
                    @endforeach
                </div>
                </div>
            </div>
        </div>
    </section>
    @include('cases_charts')
    @if(count($updates) != 0)
    <section class="categories">
        <div class="weekly-news-area pt-50">
            <div class="container">
                <div class="weekly-wrapper">
                    <!-- section Tittle -->
                    <div class="col-lg-12">
                        <div class="section-tittle mb-30">
                            <h3>We take action of what we can get on your donation.</h3>
                        </div>
                    </div>
                    <div class="row">
                    @foreach($updates as $update)
                    <div class="col-md-4">
                    <div class="card mb-4 shadow-sm">
                        <img src="{{ route('update.image',App\Common::getFirtImage($update->id)) }}"  width="100%" height="225">
                        <div class="card-body">
                        <p><b>{{ $update->title }}</b></p>
                        <div class="card-text content_data">{{ $update->description }}</div>
                        <div class="d-flex justify-content-between align-items-center">
                            <a href="{{ route('update.show', $update->id) }}" class="stretched-link">Continue reading</a>
                            <small class="text-muted">@php echo App\Common::get_time_difference_php($update->date_posted); @endphp</small>
                        </div>
                        </div>
                    </div>
                    </div>
                    @endforeach
                </div>
                </div>
            </div>
        </div>
    </section>
    @endIf
    <!-- <section class="sponsor-style-two" style="padding-bottom: 8em; padding-top: 2em; background-color: white;">
        <div class="auto-container">
            <div class="section-title text-center">
                <h1>Our Sponsors</h1>
                <h4>Check Who Makes This Fundraising Possible!</h4>
                <div class="divider">
                    <span class="left-divider"></span><span class="right-divider"></span>
                </div>
            </div>
            <div class="platinum-sponsor text-center">
                <div class="row">
                    <div class="col-md-4">
                        <img src="images/sponsors/2.png" alt="Awesome Image">
                    </div>
                    <div class="col-md-4">
                        <img src="images/sponsors/5.png" alt="Awesome Image">
                    </div>
                    <div class="col-md-4">
                        <img src="images/sponsors/4.png" alt="Awesome Image">
                    </div>
                </div>
            </div>
            <br> <br>
            <div class="gold-sponsor text-center">
                <div class="row">
                    <div class="col-md-3">
                        <img src="images/sponsors/1.png" alt="Awesome Image">
                    </div>
                    <div class="col-md-3">
                        <img src="images/sponsors/3.png" alt="Awesome Image">
                    </div>
                    <div class="col-md-3">
                        <img src="images/sponsors/6.png" alt="Awesome Image">
                    </div>
                    <div class="col-md-3">
                        <img src="images/sponsors/7.png" alt="Awesome Image">
                    </div>
                </div>
            </div>
        </div>
    </section> -->
    

    

{{--<div class="container">--}}
    {{--<div class="row justify-content-center">--}}
        {{--<div class="col-md-8">--}}
            {{--<div class="card">--}}
                {{--<div class="card-header">Dashboard</div>--}}
                {{--<h1>Confirmed Cases: {{ $confirmed ?? '' }}</h1>--}}
                {{--<h2>Recovered Cases: {{$recovered ?? ''}}</h2>--}}
                {{--<h3>Death Cases: {{$deaths ??''}}</h3>--}}
                {{--<div class="card-body">--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}
@endsection
<style>
.content_data{
  text-overflow: ellipsis;
  overflow: hidden;
  height: 87px;
}

.auto-container {
    position: static;
    max-width: 1200px;
    padding: 0px 15px;
    margin: 0 auto;
}
.section-title {
    position: relative;
    margin-bottom: 50px;
}
.section-title h5 {
    position: relative;
    font-size: 14px;
    font-family: 'Merriweather', serif;
    font-weight: 400;
    font-style: italic;
    color: #999999;
    margin-bottom: 9px;
}
.section-title h1 {
    position: relative;
    font-size: 40px;
    font-family: 'Montserrat', sans-serif;
    font-weight: 700;
    color: #000;
    text-transform: uppercase;
    margin-bottom: 13px;
}
.section-title .divider {
    position: relative;
    height: 19px;
    width: 170px;
    display: inline-block;
}
.sponsor-style-two h3 {
    margin-bottom: 20px;
    font-size: 28px;
}

</style>
