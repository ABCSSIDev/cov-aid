@extends('layouts.app')

@section('content')
{{--<div class="container" style="padding-top: 8em; padding-bottom: 3em">--}}
    {{--<div class="row justify-content-center">--}}
        {{--<div class="col-md-8">--}}
            {{--<div class="card">--}}
                {{--<div class="card-header">{{ __('Create') }}</div>--}}
                {{--<div class="card-body">--}}
                    {{--<form method="POST" action="{{ route('updates.store') }}" id="submit_updates" class="form-horizontal" enctype="multipart/form-data">--}}
                        {{--@csrf--}}
                        {{--<div class="form-group row">--}}
                            {{--<label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Title') }}</label>--}}
                            {{--<div class="col-md-6">--}}
                                {{--<select name="project" class="form-control" >--}}
                                    {{--<option value="1">PPE</option>--}}
                                    {{--<option value="2">Relief</option>--}}
                                    {{--<option value="3">Perishables</option>--}}

                                {{--</select>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="form-group row">--}}
                            {{--<label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Title') }}</label>--}}
                            {{--<div class="col-md-6">--}}
                                {{--<input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="title" value="{{ old('name') }}" required autocomplete="name" autofocus>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="form-group row">--}}
                            {{--<label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Description') }}</label>--}}
                            {{--<div class="col-md-6">--}}
                                {{--<textarea class="form-control" name="description" aria-label="With textarea"></textarea>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="form-group row">--}}
                            {{--<label for="email" class="col-md-4 col-form-label text-md-right">Date Posted:</label>--}}
                            {{--<div class="col-md-6">--}}
                                {{--<input id="date_verified" type="text" class="form-control datepicker" name="date_posted">--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="container">--}}
                            {{--<label for="email" class="col-md-4 col-form-label text-md-right">Images:</label>--}}
                            {{--<div class="row" id="upload-panel">--}}
                                {{--<div class="col-md-3 col-sm-6 upimage">--}}
                                    {{--<div class="image-editor0">--}}
                                        {{--<input type="hidden" name="items[]" value="0">--}}
                                        {{--<div class="row">--}}
                                            {{--<input type="file" class="cropit-image-input">--}}
                                        {{--</div>--}}
                                        {{--<div class="cropit-preview"></div>--}}
                                        {{--<div class="image-size-label">--}}
                                            {{--Resize image--}}
                                        {{--</div>--}}
                                        {{--<input type="range" class="cropit-image-zoom-input">--}}
                                        {{--<input type="hidden" name="image-data0" class="hidden-image-data0" />--}}
                                        {{--<input type="text" class="form-control" name="sort_order0" placeholder="Sort Order" data-validation="required">--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-3 col-sm-6">--}}
                                    {{--<div id="add-image-upload" class="cropit-preview-add"></div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group row mb-0">--}}
                            {{--<div class="col-md-6 offset-md-4">--}}
                                {{--<button type="submit" class="btn btn-primary" id="submit_product">--}}
                                    {{--{{ __('Submit') }}--}}
                                {{--</button>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}

<div class="container" style="padding-top:8em; padding-bottom:3em; ">
    <div class="card">
        <div class="card-header"><strong>{{ __('Post Donation Updates') }}</strong></div>

        <div class="card-body">

            <h4 class="form-head"></h4>
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div align="right" style="margin: 0px 0px 20px 0px">
                        <a href="{{ route('donation.index') }}">
                            <button class="btn btn-secondary"><i class="fa fa-chevron-left" aria-hidden="true"></i> BACK</button></a>
                    </div>
                </div>
            </div>
            <form method="POST" action="{{ route('updates.store') }}" id="donation_form" class="form-horizontal" enctype="multipart/form-data">
                @csrf
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label for="project" class="col-md-2 col-form-label text-md-right">{{ __('Project') }}</label>
                            <div class="col-md-6">
                                <select name="project" class="form-control" >
                                    <option value="1">PPE</option>
                                    <option value="2">Relief</option>
                                    <option value="3">Perishables</option>

                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="title" class="col-md-2 col-form-label text-md-right">{{ __('Title') }}</label>
                            <div class="col-md-8">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="title" value="{{ old('name') }}" required autocomplete="name" autofocus>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="date_posted" class="col-md-2 col-form-label text-md-right">Date Posted:</label>
                            <div class="col-md-8">
                                <input id="date_verified" type="text" class="form-control datetimepicker" name="date_posted">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="description" class="col-md-2 col-form-label text-md-right">{{ __('Description') }}</label>
                            <div class="col-md-10" style="height: 200px">
                                <textarea class="form-control" name="description" aria-label="With textarea" ></textarea>
                            </div>
                        </div>


                        <div class="container">
                            <label for="email" class="col-md-1 col-form-label text-md-right">Images:</label>
                            <div class="row" id="upload-panel">
                                <div class="col-md-3 col-sm-6 upimage">
                                    <div class="image-editor0">
                                        <input type="hidden" name="items[]" value="0">
                                        <div class="row">
                                            <input type="file" class="cropit-image-input">
                                        </div>
                                        <div class="cropit-preview"></div>
                                        <div class="image-size-label">
                                            Resize image
                                        </div>
                                        <input type="range" class="cropit-image-zoom-input">
                                        <input type="hidden" name="image-data0" class="hidden-image-data0" />
                                        <input type="text" class="form-control" name="sort_order0" placeholder="Sort Order" data-validation="required">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div id="add-image-upload" class="cropit-preview-add"></div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="form-group" align="right" >
                            <button type="submit" class="btn btn-primary" id="submit_product"><i class="fa fa-floppy-o" aria-hidden="true" ></i> SUBMIT</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
<style>
    .cropit-preview {
        background-color: #f8f8f8;
        background-size: cover;
        border: 1px solid #ccc;
        border-radius: 3px;
        margin-top: 7px;
        width: 255px;
        height: 240px;
    }


    #delete_div{
        position: absolute;
        margin: 2px;
        z-index: 1;
    }

    .delete_{
        position: absolute;
        margin: 2px;
        z-index: 1;
    }

    .cropit-preview-image-container {
        cursor: move;
    }

    .image-size-label {
        margin-top: 10px;
    }

    input {
        display: block;
    }

    button[type="submit"] {
        margin-top: 10px;
    }

    #result {
        margin-top: 10px;
        width: 900px;
    }

    #result-data {
        display: block;
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
        word-wrap: break-word;
    }

    .cropit-preview-add {
        background-color: #f8f8f8;
        background-size: cover;
        background-image: url('/image/add_image.jpg');
        border: 1px solid #ccc;
        border-radius: 3px;
        margin-top: 35px;
        width: 255px;
        height: 240px;
    }

    .image_uploaded{
        background-color: #f8f8f8;
        background-size: cover;
        border: 1px solid #ccc;
        border-radius: 3px;
        margin-top: 35px;
        width: 255px;
        height: 240px;
    }
</style>
@push('scripts')

    <script>
        $(function () {
            //function to load bootstrap datetimepicker
            $('.datetimepicker').datetimepicker();
        });
        $(function() {
            $('.image-editor0').cropit({minZoom: 2});
            $('#submit_product').click(function() {
                for (let i = 0; i < $('.upimage').length; i++) {
                    // Move cropped image data to hidden input
                    $('.hidden-image-data'+i).val($('.image-editor'+i).cropit('export',{originalSize: true,quality: 0.5}));
                }
                var formValue = $(this).serialize();
                $('#result-data').text(formValue);
                // Prevent the form from actually submitting
                return true;
            });
        });
        var ctr = 1;
        $("#add-image-upload").click(function(e){
            e.preventDefault();
            $('#upload-panel').prepend(
                '<div class="col-md-3 col-sm-6 upimage">'+
                '<input type="hidden" name="items[]" value="'+ctr+'">'+
                '<div class="image-editor'+ctr+'">'+
                '<input type="file" class="cropit-image-input">'+
                '<div class="cropit-preview"><button type="button" class="btn btn-danger" id="delete_div"><i  class="fa fa-times-circle"  aria-hidden="true"></i></button></div>'+
                '<div class="image-size-label">Resize image'+
                '</div>'+
                '<input type="range" class="cropit-image-zoom-input">'+
                '<input type="hidden" name="image-data'+ctr+'" class="hidden-image-data'+ctr+'" />'+
                '<input type="text" class="form-control" name="sort_order'+ctr+'" placeholder="Sort Order" data-validation="required">'+
                '</div>'+
                '<script>$(".image-editor'+ctr+'").cropit({minZoom: 2});<\/script>'+
                '</div>');


            $('#delete_div').click(function(e){
                $(this).closest('.upimage').remove();
                ctr--;
                return false;
            })
            ctr++;
        });

        

        $('#submit_updates').registerFields();
    </script>
@endpush
