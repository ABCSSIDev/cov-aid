@extends('layouts.app')

@section('content')
<section class="jumbotron text-center" style="background-color:white;padding-top: 5em">
    <div class="container" style="padding-top: 7em">
      <h1>Fund Raising For Fellow Filipinos</h1>
      <p class="lead text-muted"><b>DECON BUSINESS SOLUTIONS</b> who specializes with Crowdfunding and Financing will Partner up with <b>ABC SYSTEM SOLUTIONS INC.</b> one of the most promising company when it comes to Web Development and Design, Solar & Suveillance Systems and Automation of Enterprise System.</p>
      <p>
        <a href="{{ route('donate.index') }}" class="button-medium">Donate Now</a>
      </p>
      @if(Auth::check())
        @if(Auth::User()->type == 1)
      <p>
          <a href="{{ route('updates.create') }}" class="button-medium">Add Updates</a>
      </p>
        @endIf
      @endif
    </div>
</section>
<div class="album py-5 bg-light">
    <div class="container">

      <div class="row">
        @foreach($projects as $project)
        <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
            <img src="{{ $project[0]['image_thumb'] }}"  width="100%" height="225">
            <div class="card-body">
              <p><b>{{ $project[0]['title'] }}</b></p>
              <div class="card-text content_data">{!! $project[0]['content_html'] !!}</div>
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                    <a href="{{ route('fundraising.show',$project[0]['id']) }}" class="button-medium">View</a>
                </div>
                <small class="text-muted">April 1, 2020</small>
              </div>
            </div>
          </div>
        </div>
        @endforeach
      </div>
    </div>
  </div>
@endsection
<style>
.content_data{
  text-overflow: ellipsis;
  overflow: hidden;
  height: 87px;
}
</style>
