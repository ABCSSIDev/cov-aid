<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDonationProofsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donation_proofs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('donation_id');
            $table->string('attachment');
            $table->unsignedBigInteger('verified_by')->nullable();
            $table->date('verified_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('donation_id')->references('id')->on('donations')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('verified_by')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('donation_proofs');
    }
}
