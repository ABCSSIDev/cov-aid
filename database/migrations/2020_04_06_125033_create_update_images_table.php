<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUpdateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('update_images', function (Blueprint $table) {
            $table->id();
            $table->string('subject');
            $table->longText('image_url');
            $table->unsignedBigInteger('update_id');
            $table->integer('sort_order');
            $table->foreign('update_id')->references('id')->on('updates')->onUpdate('cascade')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('update_images');
    }
}
