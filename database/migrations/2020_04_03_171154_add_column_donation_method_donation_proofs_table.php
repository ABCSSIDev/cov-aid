<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnDonationMethodDonationProofsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('donation_proofs', function (Blueprint $table) {
            $table->string('donation_method')->after('attachment')->nullable()->comment('1 = GCash, 2 = BPI, 3 = Metrobank, 4 = PayPal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('disbursement_vouchers', function (Blueprint $table) {
            //
            $table->dropColumn('donation_method');
        });
    }
}
